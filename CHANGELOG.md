# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.0.0 - 2024-10-02(08:53:23 +0000)

### Other

- Implement unprivileged containers
- Cthulhu API validation
- DUStateChange! event missing on Update()

## Release v2.2.0 - 2024-07-17(13:19:26 +0000)

### Other

- Create USP Controller instance when a container added

## Release v2.1.3 - 2024-05-16(14:50:01 +0000)

### Other

- fix memory leaks and uninitialized values

## Release v2.1.2 - 2024-01-12(14:35:41 +0000)

### Other

- disable dmproxy code since it might cause crashes

## Release v2.1.1 - 2024-01-12(14:09:52 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v2.1.0 - 2023-12-15(13:30:55 +0000)

### Other

- Service discovery - Advertisement via DNS.SD

## Release v2.0.3 - 2023-12-08(09:33:52 +0000)

### Other

- Initialize return variables

## Release v2.0.2 - 2023-10-04(19:46:06 +0000)

### Other

- Implement ModuleVersion parameter

## Release v2.0.1 - 2023-10-04(13:15:47 +0000)

### Other

- Opensource component

## Release v2.0.0 - 2023-09-11(14:13:00 +0000)

### Other

- EE ModifyConstraints not aligned with standard

## Release v1.2.1 - 2023-09-07(12:19:53 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v1.2.0 - 2023-07-28(08:06:10 +0000)

### Other

- SoftwareModules.ExecutionUnit.{i}.HostObject is missing in datamodel

## Release v1.1.2 - 2023-07-12(13:58:45 +0000)

### Other

- Add global networkconfig

## Release v1.1.1 - 2023-07-05(08:31:16 +0000)

### Other

- Update NetworkConfig in datamodel

## Release v1.1.0 - 2023-06-19(13:36:46 +0000)

### Other

- pass NetworkConfig parameters to Cthulhu

## Release v1.0.0 - 2023-06-02(13:48:32 +0000)

### Other

- DU/EU Unique Identifier

## Release v0.5.0 - 2023-04-17(07:37:08 +0000)

### Other

- Implement HostObjects

## Release v0.4.13 - 2023-03-24(10:07:33 +0000)

### Other

- [timingila-cthulhu] In update path dont delete du in rlyeh if the versions between old and new are the same

## Release v0.4.12 - 2023-03-14(20:12:30 +0000)

### Other

- [Cthulhu] upgrading container when not enough space on disk...

## Release v0.4.11 - 2023-03-01(09:17:17 +0000)

### Other

- [LCM] Expose time for firstInstall and lastupdate

## Release v0.4.10 - 2023-02-27(10:13:51 +0000)

### Other

- [timingila] Use STAGING_LIBDIR instead of LIBDIR in src/makefile

## Release v0.4.9 - 2023-02-24(14:36:55 +0000)

### Other

- DU installation failure should be reported as such

## Release v0.4.8 - 2023-02-13(13:38:39 +0000)

### Other

- during Update restrictions should be updatable

## Release v0.4.7 - 2023-02-10(11:28:01 +0000)

### Other

- EU has wrong transient state when transitioning from Active to Idle

## Release v0.4.6 - 2023-02-07(13:54:56 +0000)

### Other

- [Timingila-rlyeh] Missing information on reboot

## Release v0.4.5 - 2023-01-18(12:52:49 +0000)

### Other

- DU instance created and not destroyed when DU installation fails

## Release v0.4.4 - 2022-11-23(14:51:03 +0000)

### Other

- Cthulhu should send error messages when it fails

## Release v0.4.3 - 2022-10-25(08:05:38 +0000)

### Other

- allow for several subdirs on repo

## Release v0.4.2 - 2022-09-22(11:09:00 +0000)

## Release v0.4.1 - 2022-09-21(13:21:55 +0000)

### Other

- add AutoStart parameter to InstallDU

## Release v0.4.0 - 2022-07-05(08:56:44 +0000)

### New

- allow create and update to override resource limitations

### Other

- It is not possible to retrieve the Creation Timestamp related parameter for the EE, it does not exist

## Release v0.3.7 - 2022-06-23(15:53:15 +0000)

### Fixes

- - wait for Cthulhu to be on bus before subscribing on events

## Release v0.3.6 - 2022-06-03(10:30:19 +0000)

### Other

- [cthulhu] expose EU resource limitations in Cthulhu and Timingila

## Release v0.3.5 - 2022-05-18(10:18:07 +0000)

### Other

- : Execution Environments: Remove (and LCM-111, LCM-112)

## Release v0.3.4 - 2022-05-18(09:34:44 +0000)

### Other

- Deployment Units: Retrieve. Install Timestamp

## Release v0.3.3 - 2022-05-17(13:56:37 +0000)

### Other

- LCM-136 : Execution Environments: Add Execution Environments: Add. Notifications - Operation Complete

## Release v0.3.2 - 2022-05-02(13:12:06 +0000)

### Other

- Execution Environments: Modify. Description

## Release v0.3.1 - 2022-04-25(14:18:11 +0000)

### Other

- Expose Purge api to TR181 DM

## Release v0.3.0 - 2022-03-28(13:04:10 +0000)

### New

- Read resources from EU

## Release v0.2.8 - 2022-03-07(17:15:21 +0000)

### Other

- Command to modify resoiurce constraints of an Execution Environment

## Release v0.2.7 - 2022-02-22(19:58:06 +0000)

### Other

- [LCM] Expose in SoftwareModules the available mem, diskspace of an EE (sandbox)

## Release v0.2.6 - 2022-02-22(13:21:37 +0000)

### Fixes

- [LCM] EU broken due to change to euname

## Release v0.2.5 - 2022-02-14(14:34:12 +0000)

### Fixes

- Installing a DU using an actual UUID does not lead to the creation of the EU

## Release v0.2.4 - 2022-02-04(09:19:31 +0000)

### Other

- Execution Environments: Restart
- ExecutionUnit: Restart
- [LCM] Constrain the EE (sandbox) with mem, cpu, diskspace

## Release v0.2.3 - 2021-12-13(17:46:36 +0000)

## Release v0.2.2 - 2021-12-03(03:16:35 +0000)

### Other

- Implement DeploymentUnit Update

## Release v0.2.1 - 2021-11-23(22:24:17 +0000)

## Release v0.2.0 - 2021-10-07(11:22:33 +0000)

### New

- [Timingila] Implement DU.Uninstall()

## Release v0.1.2 - 2021-09-14(15:07:30 +0000)

## Release v0.1.1 - 2021-09-14(11:59:35 +0000)

## Release v0.1.0 - 2021-09-10(09:12:27 +0000)

## Release v0.0.1 - 2021-09-03(09:48:30 +0000)

### New

- Initial release
