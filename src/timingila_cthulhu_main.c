/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_common.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include <cthulhu/cthulhu_defines.h>

#include <lcm/lcm_assert.h>

#include <timingila/timingila_defines.h>
#include <timingila/timingila_signals.h>
#include <timingila/timingila_module.h>

#include <lcm/lcm_assert.h>

#include "timingila_cthulhu_priv.h"
#include "timingila_common.h"

#define ME "cthulhu"

#define PARAMS "parameters"

#define ERR_INTERNAL_ERR_PREFIX "Internal error: "
#define ERR_CTHULHU_CONTAINER_CREATE_STRING "Container creation failed"

#define TIMINGILA_CTHULHU_METHOD "timingilacthulhumethod"
#define TIMINGILA_CTHULHU_METHOD_UNKNOWN 0x00
#define TIMINGILA_CTHULHU_METHOD_CREATEDU 0x01
#define TIMINGILA_CTHULHU_METHOD_UPDATEDU 0x02
#define TIMINGILA_CTHULHU_METHOD_UNINSTALLDU 0x03
#define TIMINGILA_CTHULHU_METHOD_EU_SETREQSTATE 0x04
#define TIMINGILA_CTHULHU_METHOD_EE_RESTART 0x05
#define TIMINGILA_CTHULHU_METHOD_EE_MODIFY 0x06
#define TIMINGILA_CTHULHU_METHOD_EE_ADD 0x07
#define TIMINGILA_CTHULHU_METHOD_EE_DELETE 0x08
#define TIMINGILA_CTHULHU_METHOD_VALIDATEINSTALL 0x09
#define TIMINGILA_CTHULHU_METHOD_VALIDATEUPDATE 0x0a


#define CTHULHU_INFO_SIZE 31
#define SEARCHPATH_SIZE 256
#define TIMINGILA_CTHULHU_SYNC_TIMEOUT 60
#define TIMINGILA_CTHULHU_GET_TIMEOUT 5

#define LCM_CREATED_LEN 30

// late init tracker
// so cthulhu and rlyeh can properly overlay their info
static bool lateinit = false;

// track if we are subscribed to cthulhu events
static bool cthulhu_subscribed = false;

// shared object bookkeeping
static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;

// bus context for Rlyeh
static amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = NULL;

// command tracker
static amxc_llist_t cthulhu_command_llist;
static timingila_command_id_type_t cthulhu_command_id_counter = 0;

// datamodel
static amxd_dm_t* dm = NULL;

// Version tracker in cthulhu
char cthulhu_backendversion[CTHULHU_INFO_SIZE + 1];
char cthulhu_version[CTHULHU_INFO_SIZE + 1];

#ifndef CTHULHU_CMD_SB_LS
#define CTHULHU_CMD_SB_LS CTHULHU_CMD_CTR_LS
#endif

static inline amxb_bus_ctx_t* cthulhu_get_amxb_bus_ctx(void) {
    return amxb_bus_ctx_cthulhu;
}

// forward declaration
static void wait_and_connect_cthulhu(void);
static int cthulhu_get_info(bool* odlloaded);
static int remove_slot(const char* const signal_name, amxp_slot_fn_t fn);

static int update_amxb_bus_ctx_cthulhu(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Update amxb bus ctx for: " CTHULHU_DM);

    amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}


static int cthulhu_emit_signal(const char* signal_name, const amxc_var_t* const data) {
    int retval = -1;
    SAH_TRACEZ_INFO(ME, "Emitting signal: %s", signal_name);
    retval = timingila_emit_signal(signal_name, data);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldn't emit signal: %s [%d]", signal_name, retval);
    }
    return retval;
}

static void emit_rlyeh_uninstall(const char* duid, const char* version) {
    amxc_var_t delete_data;
    amxc_var_init(&delete_data);
    amxc_var_set_type(&delete_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &delete_data, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, &delete_data, SM_DM_DU_VERSION, version);
    cthulhu_emit_signal(EVENT_EXEC_PACKAGER_UNINSTALL_DU, &delete_data);
    amxc_var_clean(&delete_data);
}

static void cthulhu_empty_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                  amxb_request_t* req,
                                  int status,
                                  UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " empty request is done - status = %d", status);
    amxb_close_request(&req);
}

// Generic callback to be used to clean up the structures
static void cthulhu_default_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                    amxb_request_t* req,
                                    int status,
                                    void* priv) {
    cthulhu_empty_done_cb(NULL, req, status, NULL);
    if(priv) {
        amxc_var_t* args = (amxc_var_t*) priv;
        amxc_var_delete(&args);
    }
    return;
}

static void cthulhu_sb_create_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                      amxb_request_t* req,
                                      int status,
                                      void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " create request is done - status = %d", status);
    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_ADD_EE_FAILED, (const amxc_var_t*) priv);
    } else {
        cthulhu_emit_signal(EVENT_CONTAINER_ADD_EE_DONE, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_sb_delete_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                      amxb_request_t* req,
                                      int status,
                                      void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " create request is done - status = %d", status);
    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_DELETE_EE_FAILED, (const amxc_var_t*) priv);
    } else {
        cthulhu_emit_signal(EVENT_CONTAINER_DELETE_EE_DONE, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_sb_restart_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                       amxb_request_t* req,
                                       int status,
                                       UNUSED void* priv) {
    cthulhu_empty_done_cb(NULL, req, status, NULL);
}

static void cthulhu_sb_modify_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                      amxb_request_t* req,
                                      int status,
                                      UNUSED void* priv) {
    cthulhu_empty_done_cb(NULL, req, status, NULL);
}


static void cthulhu_ctr_update_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                       amxb_request_t* req,
                                       int status,
                                       void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " create request is done - status = %d", status);
    // TODO: move to notify handler
    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_UPDATE_DU_FAILED, (const amxc_var_t*) priv);
    } else {
        cthulhu_emit_signal(EVENT_CONTAINER_UPDATE_DU_DONE, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_ctr_create_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                       amxb_request_t* req,
                                       int status,
                                       void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " create request is done - status = %d", status);
    // TODO: move to notify handler
    if(status != amxd_status_ok) {
        // TODO: check if deferred call needs to be returned here or in notify handler
        cthulhu_emit_signal(EVENT_CONTAINER_INSTALL_DU_FAILED, (const amxc_var_t*) priv);
    } else {
        cthulhu_emit_signal(EVENT_CONTAINER_INSTALL_DU_DONE, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_ctr_rm_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                   amxb_request_t* req,
                                   int status,
                                   UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " create request is done - status = %d", status);
    // TODO: move to notify handler
    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_UNINSTALL_DU_FAILED, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_ctr_validate_install_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                                 amxb_request_t* req,
                                                 int status,
                                                 UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " validate install request is done - status = %d", status);

    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_INSTALL_DU_FAILED, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void cthulhu_ctr_validate_update_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                                amxb_request_t* req,
                                                int status,
                                                UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, CTHULHU " validate update request is done - status = %d", status);

    if(status != amxd_status_ok) {
        cthulhu_emit_signal(EVENT_CONTAINER_UPDATE_DU_FAILED, (const amxc_var_t*) priv);
    }
    cthulhu_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static int event_dustatechange_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    retval = timingila_dm_event_execute(TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE, data, &ret);
    if(retval) {
        SAH_TRACEZ_ERROR(ME,
                         "timingila_dm_event_execute(" TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE ") return code %d",
                         retval);
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}


static int update_eu_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    retval = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_EU, data, &ret);
    if(retval) {
        SAH_TRACEZ_ERROR(ME,
                         "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_EU ") return code %d",
                         retval);
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

static int update_ee_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    retval = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_EE, data, &ret);
    if(retval) {
        SAH_TRACEZ_ERROR(ME,
                         "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_EE ") return code %d",
                         retval);
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

static int update_du_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    when_null_log(data, exit);
/*
    const char *uuid = GET_CHAR(data, SM_DM_DU_UUID);
    const char *status = GET_CHAR(data, SM_DM_DU_STATUS);

    amxc_var_t *du_args = NULL;
    amxc_var_new(&du_args);
    amxc_var_set_type(du_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_UUID, uuid);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_STATUS, status);
 */

    retval = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, data, &ret);
    if(retval) {
        SAH_TRACEZ_ERROR(ME,
                         "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_DU ") return code %d",
                         retval);
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

static void update_du_status(const char* duid, const char* version, const char* status) {
    amxc_var_t failedargs;
    amxc_var_init(&failedargs);
    amxc_var_set_type(&failedargs, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &failedargs, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, &failedargs, SM_DM_DU_VERSION, version);
    amxc_var_add_key(cstring_t, &failedargs, SM_DM_DU_STATUS, status);
    update_du_from_cthulhu(&failedargs);
    amxc_var_clean(&failedargs);
}

static void end_deferred_call_on_err(uint64_t callid, uint32_t faulttype, const char* faulttypestr, const char* reason) {
    SAH_TRACEZ_INFO(ME, "Have " AMXB_DEFERRED_CALLID " %" PRIu64 " so returning the error on the rpc: (%" PRIu32 ")", callid, faulttype);

    amxc_var_t deferred_data;
    amxc_var_init(&deferred_data);
    amxc_var_set_type(&deferred_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &deferred_data, AMXB_DEFERRED_ERR_CODE, faulttype);
    if(!string_is_empty(reason)) {
        amxc_var_add_key(cstring_t, &deferred_data, AMXB_DEFERRED_ERR_MSG, reason);
    } else {
        amxc_var_add_key(cstring_t, &deferred_data, AMXB_DEFERRED_ERR_MSG, faulttypestr);
    }
    amxd_function_deferred_done(callid, amxd_status_unknown_error, &deferred_data, NULL);
    amxc_var_clean(&deferred_data);
}

static int update_du_from_cthulhu_parsed_list(amxc_var_t* data) {
    int retval = -1;
    const char* sandbox = NULL;
    const char* bversion = NULL;
    const char* bvendor = NULL;
    const char* bname = NULL;
    const char* alias = NULL;
    const char* ctrid = NULL;
    const char* bdescription = NULL;
    const char* linkeduuid = NULL;
    const char* mversion = NULL;
    amxc_ts_t* du_created = NULL;
    amxc_ts_t* du_updated = NULL;
    amxc_var_t* args = NULL;

    when_null_log(data, exit);

    ctrid = GET_CHAR(data, CTHULHU_CONTAINER_ID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_ID);
        goto exit;
    }

    sandbox = GET_CHAR(data, CTHULHU_CONTAINER_SANDBOX);
    if(sandbox == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_SANDBOX);
        goto exit;
    }

    bversion = GET_CHAR(data, CTHULHU_CONTAINER_BUNDLEVERSION);
    if(bversion == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_BUNDLEVERSION);
        goto exit;
    }

    bdescription = GET_CHAR(data, CTHULHU_CONTAINER_DESCRIPTION);
    bvendor = GET_CHAR(data, CTHULHU_CONTAINER_VENDOR);
    bname = GET_CHAR(data, CTHULHU_CONTAINER_BUNDLE);
    alias = GET_CHAR(data, CTHULHU_CONTAINER_ALIAS);
    linkeduuid = GET_CHAR(data, CTHULHU_CONTAINER_LINKED_UUID);
    du_created = amxc_var_dyncast(amxc_ts_t, GET_ARG(data, CTHULHU_CONTAINER_CREATED));
    du_updated = amxc_var_dyncast(amxc_ts_t, GET_ARG(data, CTHULHU_CONTAINER_UPDATED));
    mversion = GET_CHAR(data, CTHULHU_CONTAINER_MODULEVERSION);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, args, SM_DM_DU_UUID, linkeduuid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, ctrid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_EE_REF, sandbox);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, bversion);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VENDOR, bvendor);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DESCRIPTION, bdescription);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_NAME, bname);
    // TODO: prefix cpe-
    amxc_var_add_key(cstring_t, args, SM_DM_DU_ALIAS, alias);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLED);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_INSTALLED, du_created);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_LASTUPDATE, du_updated);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_MODULEVERSION, mversion);


    retval = update_du_from_cthulhu(args);
    amxc_var_delete(&args);
    free(du_created);
    free(du_updated);
exit:
    return retval;
}

static int copy_var_from_table(const amxc_var_t* const src, amxc_var_t* dest, const char* const key) {
    int retval = -1;
    amxc_var_t* src_var = amxc_var_get_key(src, key, AMXC_VAR_FLAG_DEFAULT);
    if(!src_var) {
        retval = 0;
        goto exit;
    }
    amxc_var_t* dest_var = amxc_var_add_new_key(dest, key);
    when_null(dest_var, exit);
    retval = amxc_var_copy(dest_var, src_var);
exit:
    return retval;
}

static int eu_add_plugin_data(amxc_var_t* args, const amxc_var_t* const plugins) {
    int retval = -1;
    if(!plugins) {
        retval = 0;
        goto exit;
    }
    amxc_var_t* networkconfig = GET_ARG(plugins, SM_DM_EU_NC);
    if(networkconfig) {
        amxc_var_t* nc_new = amxc_var_add_new_key_amxc_htable_t(args, SM_DM_EU_NC, NULL);
        when_null(nc_new, exit);
        amxc_var_t* itfs = GET_ARG(networkconfig, SM_DM_EU_NC_ACCESSINTERFACES);
        if(itfs) {
            amxc_var_t* itfs_new = amxc_var_add_new_key_amxc_htable_t(nc_new, SM_DM_EU_NC_ACCESSINTERFACES, NULL);
            amxc_var_for_each(itf, itfs) {
                const char* key = amxc_var_key(itf);
                if(!string_is_empty(key)) {
                    amxc_var_t* itf_new = amxc_var_add_new_key_amxc_htable_t(itfs_new, key, NULL);
                    copy_var_from_table(itf, itf_new, SM_DM_EU_NC_ACCESSINTERFACES_REFERENCE);
                }
            }
        }
        amxc_var_t* pfs = GET_ARG(networkconfig, SM_DM_EU_NC_PORTFORWARDING);
        if(pfs) {
            amxc_var_t* pfs_new = amxc_var_add_new_key_amxc_htable_t(nc_new, SM_DM_EU_NC_PORTFORWARDING, NULL);
            amxc_var_for_each(pf, pfs) {
                const char* key = amxc_var_key(pf);
                if(!string_is_empty(key)) {
                    amxc_var_t* pf_new = amxc_var_add_new_key_amxc_htable_t(pfs_new, key, NULL);
                    copy_var_from_table(pf, pf_new, SM_DM_EU_NC_PORTFORWARDING_INTERFACE);
                    copy_var_from_table(pf, pf_new, SM_DM_EU_NC_PORTFORWARDING_PROTOCOL);
                    copy_var_from_table(pf, pf_new, SM_DM_EU_NC_PORTFORWARDING_EXTERNALPORT);
                    copy_var_from_table(pf, pf_new, SM_DM_EU_NC_PORTFORWARDING_INTERNALPORT);
                }
            }
        }
        amxc_var_t* dnssds = GET_ARG(networkconfig, "DNSSD");
        if(dnssds && amxc_var_get_first(dnssds)) {
            amxc_llist_t dnssdreflist;
            amxc_llist_init(&dnssdreflist);
            amxc_var_for_each(dnssd, dnssds) {
                const char* path = GET_CHAR(dnssd, "Path");
                if(!string_is_empty(path)) {
                    amxc_llist_add_string(&dnssdreflist, path);
                }
            }
            amxc_string_t dnssdreflist_csv;
            amxc_string_init(&dnssdreflist_csv, 0);
            amxc_string_join_llist(&dnssdreflist_csv, &dnssdreflist, ',');
            amxc_var_add_new_key_csv_string_t(nc_new, SM_DM_EU_NC_DNSSDREFLIST, amxc_string_get(&dnssdreflist_csv, 0));
            amxc_string_clean(&dnssdreflist_csv);
            amxc_llist_clean(&dnssdreflist, amxc_string_list_it_free);
        } else {
            amxc_var_add_new_key_csv_string_t(nc_new, SM_DM_EU_NC_DNSSDREFLIST, "");
        }
    }
    retval = 0;
exit:
    return retval;
}

static int eu_add_hostobject_data(amxc_var_t* args, const amxc_var_t* const hostobjects) {
    int retval = -1;
    if(!hostobjects) {
        retval = 0;
        goto exit;
    }
    amxc_var_t* hostobjects_new = amxc_var_add_new_key_amxc_htable_t(args, SM_DM_EU_HOSTOBJECT, NULL);
    amxc_var_for_each(hostobject, hostobjects) {
        const char* key = amxc_var_key(hostobject);
        if(!string_is_empty(key)) {
            amxc_var_t* hostobject_new = amxc_var_add_new_key_amxc_htable_t(hostobjects_new, key, NULL);
            copy_var_from_table(hostobject, hostobject_new, SM_DM_EU_HOSTOBJECT_SOURCE);
            copy_var_from_table(hostobject, hostobject_new, SM_DM_EU_HOSTOBJECT_DESTINATION);
            copy_var_from_table(hostobject, hostobject_new, SM_DM_EU_HOSTOBJECT_OPTIONS);
        }
    }
    retval = 0;
exit:
    return retval;
}

/**
 *@brief      eu_add_environment_variables_data
 *
 *@detail     Update Environment Variables to EU Data Model
 *
 *@param[in]  args, contains the information about container
 *            envvars, contains environment variables
 */
static int eu_add_environment_variables_data(amxc_var_t* args, const amxc_var_t* const envvars) {
    int retval = -1;
    if(!envvars) {
        retval = 0;
        goto exit;
    }
    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        retval = 0;
        goto exit;
    }
    amxc_var_t* envvars_new = amxc_var_add_new_key_amxc_htable_t(args, SM_DM_EU_ENVIRONMENTVARIABLES, NULL);
    amxc_var_for_each(envvar, envvars) {
        const char* key = amxc_var_key(envvar);
        if(!string_is_empty(key)) {
            amxc_var_t* envvar_new = amxc_var_add_new_key_amxc_htable_t(envvars_new, key, NULL);
            copy_var_from_table(envvar, envvar_new, SM_DM_EU_ENVIRONMENTVARIABLES_KEY);
            copy_var_from_table(envvar, envvar_new, SM_DM_EU_ENVIRONMENTVARIABLES_VALUE);
        }
    }
    retval = 0;
exit:
    return retval;
}

/**
 *@brief      eu_add_autorestart
 *
 *@detail     Update AutoRestart to EU Data Model
 *
 *@param[in]  args, contains the information about container
 *            autorestart, contains the information about autorestart
 */
static int eu_add_autorestart(amxc_var_t* args, const amxc_var_t* const autorestart) {
    int retval = -1;
    if(!autorestart) {
        retval = 0;
        goto exit;
    }

    amxc_var_t* auto_htable = amxc_var_add_key(amxc_htable_t, args, SM_DM_EU_AUTORESTART, NULL);
    if(autorestart != NULL) {
        bool enable = GET_BOOL(autorestart, CTHULHU_DM_CTR_AR_ENABLED);
        amxc_var_add_key(bool, auto_htable, SM_DM_EU_AUTORESTART_ENABLE, enable);

        uint32_t max_retry_count = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_MAX_RETRYCOUNT);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_MAX_RETRYCOUNT, max_retry_count);

        uint32_t minimum_interval = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_MIN_INT);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_MIN_INT, minimum_interval);

        uint32_t maximum_interval = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_MAX_INT);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_MAX_INT, maximum_interval);

        uint32_t retry_interval = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_RETRY_INT);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_RETRY_INT, retry_interval);

        uint32_t reset_period = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_RESET);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_RESET, reset_period);

        uint32_t retry_count = GET_UINT32(autorestart, SM_DM_EU_AUTORESTART_RETRY_COUNT);
        amxc_var_add_key(uint32_t, auto_htable, SM_DM_EU_AUTORESTART_RETRY_COUNT, retry_count);

        const char* last_retry = GET_CHAR(autorestart, SM_DM_EU_AUTORESTART_LASTRESTART);
        amxc_var_add_key(cstring_t, auto_htable, SM_DM_EU_AUTORESTART_LASTRESTART, last_retry);

        const char* next_retry = GET_CHAR(autorestart, SM_DM_EU_AUTORESTART_NEXTRESTART);
        amxc_var_add_key(cstring_t, auto_htable, SM_DM_EU_AUTORESTART_NEXTRESTART, next_retry);
    }

    retval = 0;
exit:
    return retval;
}

static int update_eu_from_cthulhu_parsed(amxc_var_t* data, UNUSED amxc_var_t* changed) {
    int retval = -1;
    const char* ctrid = NULL;
    const char* ctralias = NULL;
    const char* sandbox = NULL;
    const char* ctrstatus = NULL;
    const char* eustatus = NULL;
    const char* eufcode = NULL;
    const char* eufstr = NULL;
    const char* bversion = NULL;
    const char* bvendor = NULL;
    const char* bname = NULL;
    char bname_truncated[32] = {'\0'};
    const char* bdescription = NULL;
    const char emptystr[1] = {'\0'};
//    bool autorestartenabled = false;
    bool autostart = false;
    amxc_var_t* plugins = NULL;
    amxc_var_t* hostobjects = NULL;
    amxc_var_t* args = NULL;
    amxc_var_t* envvars = NULL;
    const char* creation_time = NULL;
    amxc_var_t* var = NULL;
    uint32_t uint32 = 0;
    amxc_var_t* autorestart = NULL;

    when_null_log(data, exit);

    ctrid = GET_CHAR(data, CTHULHU_CONTAINER_ID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_ID);
        goto exit;
    }

    bdescription = GET_CHAR(data, CTHULHU_CONTAINER_DESCRIPTION);
    bvendor = GET_CHAR(data, CTHULHU_CONTAINER_VENDOR);
    bversion = GET_CHAR(data, CTHULHU_CONTAINER_BUNDLEVERSION);
    bname = GET_CHAR(data, CTHULHU_CONTAINER_BUNDLE);
    if(bname) {
        strncpy(bname_truncated, bname, 31);
    }

    ctralias = GET_CHAR(data, CTHULHU_CONTAINER_ALIAS);
    sandbox = GET_CHAR(data, CTHULHU_CONTAINER_SANDBOX);
    ctrstatus = GET_CHAR(data, CTHULHU_CONTAINER_STATUS);
    if(ctrstatus) {
        if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_STARTING) == 0) {
            eustatus = SM_DM_EU_STATUS_STARTING;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_RUNNING) == 0) {
            eustatus = SM_DM_EU_STATUS_ACTIVE;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_STOPPING) == 0) {
            eustatus = SM_DM_EU_STATUS_STOPPING;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_STOPPED) == 0) {
            eustatus = SM_DM_EU_STATUS_IDLE;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_RESTARTING) == 0) {
            eustatus = SM_DM_EU_STATUS_RESTARTING;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_CREATED) == 0) {
            eustatus = SM_DM_EU_STATUS_IDLE;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_GHOST) == 0) {
            eustatus = SM_DM_EU_STATUS_IDLE;
            eufcode = SM_DM_EU_EXEC_F_CODE_US;
            eufstr = CTHULHU ": " CTHULHU_CONTAINER_STATUS_GHOST;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_UNKNOWN) == 0) {
            eustatus = SM_DM_EU_STATUS_IDLE;
            eufcode = SM_DM_EU_EXEC_F_CODE_US;
            eufstr = CTHULHU ": " CTHULHU_CONTAINER_STATUS_UNKNOWN;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_ABORTING) == 0) {
            eustatus = SM_DM_EU_STATUS_STOPPING;
            // TODO: if autostart
            eufcode = SM_DM_EU_EXEC_F_CODE_START_FO;
            eufstr = CTHULHU ": " CTHULHU_CONTAINER_STATUS_ABORTING;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_FREEZING) == 0) {
            eustatus = SM_DM_EU_STATUS_STOPPING;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_FROZEN) == 0) {
            eustatus = SM_DM_EU_STATUS_IDLE;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        } else if(strcmp(ctrstatus, CTHULHU_CONTAINER_STATUS_THAWED) == 0) {
            eustatus = SM_DM_EU_STATUS_ACTIVE;
            eufcode = SM_DM_EU_EXEC_F_CODE_NOF;
            eufstr = emptystr;
        }
    }

    autostart = GET_BOOL(data, CTHULHU_CONTAINER_AUTOSTART);
    plugins = GET_ARG(data, CTHULHU_PLUGINS_PRIVATE);
    hostobjects = GET_ARG(data, CTHULHU_CONTAINER_HOSTOBJECT);
    envvars = GET_ARG(data, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    autorestart = GET_ARG(data, SM_DM_EU_AUTORESTART);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EUID, ctrid);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_NAME, bname_truncated);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EE_LABEL, ctrid);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_ALIAS, ctralias);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EE_REF, sandbox);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_STATUS, eustatus);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EXEC_F_CODE, eufcode);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EXEC_F_MSG, eufstr);
    amxc_var_add_key(bool, args, SM_DM_EU_AUTOSTART, autostart);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_VERSION, bversion);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_VENDOR, bvendor);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_DESCRIPTION, bdescription);
    eu_add_hostobject_data(args, hostobjects);
    eu_add_plugin_data(args, plugins);
    eu_add_environment_variables_data(args, envvars);
    eu_add_autorestart(args, autorestart);

    /* container's creation time is extracted from the 'Created' parameter */
    creation_time = GET_CHAR(data, CTHULHU_CONTAINER_CREATED);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_CREATED_TIME, creation_time);

    var = GET_ARG(data, CTHULHU_CONTAINER_UNPRIVILEGED);
    if(var) {
        uint32 = GET_UINT32(var, CTHULHU_CONTAINER_UNPRIVILEGED_UID);
        amxc_var_add_key(uint32_t, args, SM_DM_EU_ALLOCATEDHOSTUID, uint32);
        uint32 = GET_UINT32(var, CTHULHU_CONTAINER_UNPRIVILEGED_GID);
        amxc_var_add_key(uint32_t, args, SM_DM_EU_ALLOCATEDHOSTGID, uint32);
    }
    retval = update_eu_from_cthulhu(args);
    amxc_var_delete(&args);
exit:
    return retval;
}

/**
 *@brief ee_add_applicationdata
 *
 *@detail     Update ApplicationData to EU Data Model
 *
 *@param[in]  args, contains the information about container
 *            applicationdata, contains applicationdata variables
 */
static int ee_add_applicationdata(amxc_var_t* args, const amxc_var_t* const applicationdata) {
    int retval = -1;
    if(!applicationdata) {
        retval = 0;
        goto exit;
    }
    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        retval = 0;
        goto exit;
    }
    amxc_var_t* applicationdata_new = amxc_var_add_new_key_amxc_htable_t(args, SM_DM_EE_APPLICATIONDATA, NULL);
    amxc_var_for_each(appdata, applicationdata) {
        const char* key = amxc_var_key(appdata);
        if(!string_is_empty(key)) {
            amxc_var_t* appdata_new = amxc_var_add_new_key_amxc_htable_t(applicationdata_new, key, NULL);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_NAME);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_CAPACITY);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_ENCRYPTED);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_RETAIN);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_ACCESSPATH);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_ALIAS);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_APPLICATIONUUID);
            copy_var_from_table(appdata, appdata_new, SM_DM_EE_APPLICATIONDATA_UTILIZATION);
        }
    }
    retval = 0;
exit:
    return retval;
}

static int update_ee_from_cthulhu_parsed(amxc_var_t* data, UNUSED amxc_var_t* changed) {
    int retval = -1;
    const char* sbid = NULL;
    const char* sbalias = NULL;
    const char* sbstatus = NULL;
    const char* sbrestartreason = NULL;
    const char* sbparent = NULL;
    const char* sbversion = NULL;
    const char* sbvendor = NULL;
    const char* sbdescription = NULL;
    const char* sbavailableroles = NULL;
    amxc_ts_t* sbcreated = NULL;
    char sbdescription_truncated[256] = {'\0'};
    int sbmemory = -1;
    int sbdiskspace = -1;
    int sbcpu = -1;
    bool sbenable = false;
    bool sbcreatedbycontainer = false;
    amxc_var_t* applicationdata = NULL;
    amxc_var_t* args = NULL;

    when_null_log(data, exit);

    sbid = GET_CHAR(data, CTHULHU_SANDBOX_ID);
    if(sbid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_SANDBOX_ID);
        goto exit;
    }
    sbcreatedbycontainer = GET_BOOL(data, CTHULHU_SANDBOX_CREATEDBYCONTAINER);
    if(sbcreatedbycontainer) {
        // this is an internal sandbox
        retval = 0;
        goto exit;
    }

    sbalias = GET_CHAR(data, CTHULHU_SANDBOX_ALIAS);
    // TODO: doesnt map with eu
    sbstatus = GET_CHAR(data, CTHULHU_SANDBOX_STATUS);
    sbrestartreason = GET_CHAR(data, CTHULHU_SANDBOX_RESTART_REASON);
    sbparent = GET_CHAR(data, CTHULHU_SANDBOX_PARENT);
    sbversion = GET_CHAR(data, CTHULHU_SANDBOX_VERSION);
    sbvendor = GET_CHAR(data, CTHULHU_SANDBOX_VENDOR);
    sbenable = GET_BOOL(data, CTHULHU_SANDBOX_ENABLE);
    sbdescription = GET_CHAR(data, CTHULHU_SANDBOX_DESCRIPTION);
    sbavailableroles = GET_CHAR(data, CTHULHU_SANDBOX_AVAILABLE_ROLES);
    sbcreated = amxc_var_dyncast(amxc_ts_t, GET_ARG(data, CTHULHU_SANDBOX_CREATED));
    if(sbdescription) {
        strncpy(sbdescription_truncated, sbdescription, 255);
    }

    sbmemory = GET_INT32(data, CTHULHU_SANDBOX_MEMORY);
    sbdiskspace = GET_INT32(data, CTHULHU_SANDBOX_DISKSPACE);
    sbcpu = GET_INT32(data, CTHULHU_SANDBOX_CPU);

    applicationdata = GET_ARG(data, CTHULHU_SANDBOX_APPLICATIONDATA);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_NAME, sbid);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_ALIAS, sbalias);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_STATUS, sbstatus);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_RESTART_REASON, sbrestartreason);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_PARENT_EE, sbparent);
    amxc_var_add_key(bool, args, SM_DM_EE_ENABLE, sbenable);
    amxc_var_add_key(int32_t, args, SM_DM_EE_ALLOCMEM, sbmemory);
    amxc_var_add_key(int32_t, args, SM_DM_EE_ALLOCDISKSPACE, sbdiskspace);
    amxc_var_add_key(int32_t, args, SM_DM_EE_ALLOCCPUPERCENT, sbcpu);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_DESCRIPTION, sbdescription_truncated);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_EE_CREATEDAT, sbcreated);
    amxc_var_add_key(csv_string_t, args, SM_DM_EE_AVAILABLE_ROLES, sbavailableroles);
    // TODO: get from cthulhu

    if(!string_is_empty(sbversion)) {
        amxc_var_add_key(cstring_t, args, SM_DM_EE_VERSION, sbversion);
    } else {
        amxc_var_add_key(cstring_t, args, SM_DM_EE_VERSION, cthulhu_version);
    }

    if(!string_is_empty(sbvendor)) {
        amxc_var_add_key(cstring_t, args, SM_DM_EE_VENDOR, sbvendor);
    } else {
        amxc_var_add_key(cstring_t, args, SM_DM_EE_VENDOR, CTHULHU);
    }

    amxc_var_add_key(cstring_t, args, SM_DM_EE_TYPE, cthulhu_backendversion);

    ee_add_applicationdata(args, applicationdata);

    retval = update_ee_from_cthulhu(args);
    amxc_var_delete(&args);
    free(sbcreated);
exit:
    return retval;
}

static int remove_eu_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_REMOVE_EU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_REMOVE_EU ") return code %d",
                    amxm_ret);
    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

static int remove_ee_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_REMOVE_EE, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_REMOVE_EE ") return code %d",
                    amxm_ret);
    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

/*
   static int remove_du_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        goto exit;
    }

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_REMOVE_DU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_REMOVE_DU ") return code %d",
                    amxm_ret);

   exit:
    return retval;
   }
 */

/*
   static int remove_du_from_cthulhu_parsed(amxc_var_t* notification_parameters){
    int retval = 1;
    const char* name = NULL;
    amxc_var_t *args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        goto exit;
    }

    name = GET_CHAR(notification_parameters, CTHULHU_DM_CONTAINER_NAME);
    if( name == NULL){
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_DM_CONTAINER_NAME);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_UUID, name);

    retval = remove_du_from_cthulhu(args);
    amxc_var_delete(&args);
   exit:
    return retval;
   }
 */

static int update_du_from_cthulhu_parsed(amxc_var_t* notification_parameters) {
    int retval = 1;
    const char* ctrid = NULL;
    const char* du_status = NULL;
    const char* module_version = NULL;
    amxc_ts_t* du_created = NULL;
    amxc_ts_t* du_updated = NULL;
    amxc_var_t* args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        goto exit;
    }

    ctrid = GET_CHAR(notification_parameters, CTHULHU_CONTAINER_ID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_ID);
        goto exit;
    }

    du_status = GET_CHAR(notification_parameters, SM_DM_DU_STATUS);
    if(du_status == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_STATUS);
        goto exit;
    }

    du_created = amxc_var_dyncast(amxc_ts_t, GET_ARG(notification_parameters, CTHULHU_CONTAINER_CREATED));
    if(du_created == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_CREATED);
        goto exit;
    }

    du_updated = amxc_var_dyncast(amxc_ts_t, GET_ARG(notification_parameters, CTHULHU_CONTAINER_UPDATED));
    if(du_updated == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_UPDATED);
        goto exit;
    }

    module_version = GET_CHAR(notification_parameters, CTHULHU_CONTAINER_MODULEVERSION);
    if(module_version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_MODULEVERSION);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, ctrid);

    amxc_var_add_key(cstring_t, args, SM_DM_DU_STATUS, du_status);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_LASTUPDATE, du_updated);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_INSTALLED, du_created);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_MODULEVERSION, module_version);

    retval = update_du_from_cthulhu(args);
    amxc_var_delete(&args);
    free(du_created);
    free(du_updated);
exit:
    return retval;
}

static int update_du_from_cthulhu_parsed_create(amxc_var_t* data) {
    int retval = -1;
    const char* ctrid = NULL;
    const char* version = NULL;
    const char* du_status = NULL;
    amxc_ts_t* du_created = NULL;
    amxc_ts_t* du_updated = NULL;
    amxc_var_t* args = NULL;
    const char* module_version = NULL;

    when_null_log(data, exit);

    ctrid = GET_CHAR(data, CTHULHU_CONTAINER_ID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_ID);
        goto exit;
    }

    version = GET_CHAR(data, CTHULHU_DM_CONTAINER_BUNDLEVERSION);
    if(version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_DM_CONTAINER_BUNDLEVERSION);
        goto exit;
    }

    du_status = GET_CHAR(data, SM_DM_DU_STATUS);
    if(du_status == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_STATUS);
        goto exit;
    }

    du_created = amxc_var_dyncast(amxc_ts_t, GET_ARG(data, CTHULHU_CONTAINER_CREATED));
    if(du_created == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_CREATED);
        goto exit;
    }

    du_updated = amxc_var_dyncast(amxc_ts_t, GET_ARG(data, CTHULHU_CONTAINER_UPDATED));
    if(du_updated == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_UPDATED);
        goto exit;
    }

    module_version = GET_CHAR(data, CTHULHU_CONTAINER_MODULEVERSION);
    if(module_version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU_CONTAINER_MODULEVERSION);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, ctrid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);

    amxc_var_add_key(cstring_t, args, SM_DM_DU_STATUS, du_status);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_INSTALLED, du_created);
    amxc_var_add_key(amxc_ts_t, args, SM_DM_DU_LASTUPDATE, du_updated);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_MODULEVERSION, module_version);

    retval = update_du_from_cthulhu(args);
    free(du_created);
    free(du_updated);
    amxc_var_delete(&args);
exit:
    return retval;
}

static int remove_eu_from_cthulhu_parsed(amxc_var_t* notification_parameters) {
    int retval = 1;
    const char* ctrid = NULL;
    amxc_var_t* args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        goto exit;
    }

    ctrid = GET_CHAR(notification_parameters, CTHULHU_CONTAINER_ID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU " " CTHULHU_CONTAINER_ID);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_EU_EUID, ctrid);

    retval = remove_eu_from_cthulhu(args);
    amxc_var_delete(&args);
exit:
    return retval;
}

static int remove_ee_from_cthulhu_parsed(amxc_var_t* notification_parameters) {
    int retval = 1;
    const char* sbid = NULL;
    amxc_var_t* args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        goto exit;
    }

    sbid = GET_CHAR(notification_parameters, CTHULHU_SANDBOX_ID);
    if(sbid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " CTHULHU " " CTHULHU_SANDBOX_ID);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_EE_NAME, sbid);

    retval = remove_ee_from_cthulhu(args);
    amxc_var_delete(&args);
exit:
    return retval;
}

static void cthulhu_notif_error(timingila_command_entry_t* cmd,
                                const char* command,
                                uint32_t faulttype,
                                const char* faulttypestr,
                                const char* reason) {
    const char* uuid = GET_CHAR(cmd->priv, SM_DM_DU_UUID);
    const char* eeref = GET_CHAR(cmd->priv, SM_DM_DU_EE_REF);

    if(!string_is_empty(reason)) {
        faulttypestr = reason;
    }
    amxc_var_t* notif_data = NULL;

    amxc_var_new(&notif_data);
    amxc_var_set_type(notif_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
    amxc_var_add_key(uint32_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, faulttype);
    if(!string_is_empty(reason)) {
        amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, reason);
    } else {
        amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, faulttypestr);
    }
    amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_EE_REF, eeref);

    if(!string_is_empty(command)) {
        if(strcmp(command, CTHULHU_CMD_CTR_CREATE) == 0) {
            amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_DU_STATUS_FAILED);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL);
        } else if(strcmp(command, CTHULHU_CMD_CTR_UPDATE) == 0) {
            amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_DU_STATUS_FAILED);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UPDATE);
        } else if(strcmp(command, CTHULHU_CMD_CTR_REMOVE) == 0) {
            amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_DU_STATUS_FAILED);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UNINSTALL);
        }
    }
    event_dustatechange_from_cthulhu(notif_data);
    amxc_var_delete(&notif_data);
}

static int cthulhu_get_info(bool* initialized) {
    int ret = -1;
    amxc_var_t cthulhu_info;

    SAH_TRACEZ_INFO(ME, "Getting info on cthulhu");

    amxc_var_init(&cthulhu_info);

    ret = amxb_get(amxb_bus_ctx_cthulhu, CTHULHU_DM_INFO ".", 0, &cthulhu_info, TIMINGILA_CTHULHU_SYNC_TIMEOUT);
    if(ret == amxd_status_ok) {
        amxc_var_for_each(var, &cthulhu_info) {
            amxc_var_t* infovar = GET_ARG(var, CTHULHU_DM_INFO ".");
            const char* backvers = GET_CHAR(infovar, CTHULHU_INFO_BACKENDVERSION);
            const char* vers = GET_CHAR(infovar, CTHULHU_INFO_VERSION);
            bool initializedinternal = GET_BOOL(infovar, CTHULHU_INFO_INITIALIZED);
            if(initialized) {
                *initialized = initializedinternal;
            }
            if(backvers) {
                SAH_TRACEZ_WARNING(ME, "cthulhu_backendversion: %s", backvers);
                strncpy(cthulhu_backendversion, backvers, CTHULHU_INFO_SIZE);
            }
            if(vers) {
                SAH_TRACEZ_WARNING(ME, "cthulhu_version: %s", vers);
                strncpy(cthulhu_version, vers, CTHULHU_INFO_SIZE);
            }
            SAH_TRACEZ_WARNING(ME, "cthulhu " CTHULHU_INFO_INITIALIZED ": %d", initializedinternal);
        }
    }
    amxc_var_clean(&cthulhu_info);
    return ret;
}

static int cthulhu_sandbox_command(const char* command, amxc_var_t* args, amxb_be_done_cb_fn_t cb, void* priv) {
    int retval = -2;
    amxb_request_t* request = NULL;
    char command_id_str[SIZE_COMMAND_ID_STR];
    unsigned int command_id = 0;
    amxc_var_t* cthulhu_command_args = NULL;

    if(cb == NULL) {
        SAH_TRACEZ_ERROR(ME, "Callback is NULL");
        goto exit;
    }

    command_id = cthulhu_command_id_counter++;

    if(timingila_command_id_to_hex_str(command_id_str, command_id) < 1) {
        SAH_TRACEZ_ERROR(ME, "command_id_str might be malformed");
    }

    amxc_var_new(&cthulhu_command_args);
    amxc_var_set_type(cthulhu_command_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_command_args, CTHULHU_COMMAND_COMMAND, command);
    amxc_var_add_key(cstring_t, cthulhu_command_args, CTHULHU_NOTIF_COMMAND_ID, command_id_str);
    amxc_var_add_key(amxc_htable_t, cthulhu_command_args, CTHULHU_COMMAND_PARAMETERS, amxc_var_constcast(amxc_htable_t, args));

    SAH_TRACEZ_INFO(ME, CTHULHU " command: %s - %s", command, command_id_str);

    if(priv && timingila_command_add_new(&cthulhu_command_llist, command_id, priv)) {
        SAH_TRACEZ_WARNING(ME, "Couldn't add command");
    }

    request = amxb_async_call(cthulhu_get_amxb_bus_ctx(),
                              CTHULHU_DM_SANDBOX,
                              CTHULHU_CMD_COMMAND,
                              cthulhu_command_args,
                              cb,
                              priv);
    if(request == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot do " CTHULHU_DM_SANDBOX ".%s()", command);
        goto exit;
    }

    retval = 0;
exit:
    amxc_var_delete(&cthulhu_command_args);
    return retval;
}

static int cthulhu_container_command(const char* command, amxc_var_t* args, amxb_be_done_cb_fn_t cb, void* priv) {
    int retval = -2;
    amxb_request_t* request = NULL;
    char command_id_str[SIZE_COMMAND_ID_STR];
    unsigned int command_id = 0;
    amxc_var_t* cthulhu_command_args = NULL;

    if(cb == NULL) {
        SAH_TRACEZ_ERROR(ME, "Callback is NULL");
        goto exit;
    }

    command_id = cthulhu_command_id_counter++;

    if(timingila_command_id_to_hex_str(command_id_str, command_id) < 1) {
        SAH_TRACEZ_ERROR(ME, "command_id_str might be malformed");
    }

    amxc_var_new(&cthulhu_command_args);
    amxc_var_set_type(cthulhu_command_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_command_args, CTHULHU_COMMAND_COMMAND, command);
    amxc_var_add_key(cstring_t, cthulhu_command_args, CTHULHU_NOTIF_COMMAND_ID, command_id_str);
    amxc_var_add_key(amxc_htable_t, cthulhu_command_args, CTHULHU_COMMAND_PARAMETERS, amxc_var_constcast(amxc_htable_t, args));

    SAH_TRACEZ_INFO(ME, CTHULHU " command: %s - %s", command, command_id_str);

    if(priv && timingila_command_add_new(&cthulhu_command_llist, command_id, priv)) {
        SAH_TRACEZ_WARNING(ME, "Couldn't add command");
    }

    request = amxb_async_call(cthulhu_get_amxb_bus_ctx(),
                              CTHULHU_DM_CONTAINER,
                              CTHULHU_CMD_COMMAND,
                              cthulhu_command_args,
                              cb,
                              priv);
    if(request == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot do " CTHULHU_DM_CONTAINER ".%s()", command);
        goto exit;
    }

    retval = 0;
exit:
    amxc_var_delete(&cthulhu_command_args);
    return retval;
}

static void cthulhu_sb_list_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                    amxb_request_t* req,
                                    int status,
                                    UNUSED void* priv) {
    cthulhu_empty_done_cb(NULL, req, status, NULL);
    if(status == amxd_status_ok) {
        lateinit = true;
        cthulhu_container_command(CTHULHU_CMD_CTR_LS, NULL, cthulhu_empty_done_cb, NULL);
    }
}

static inline void end_deferred_call_on_succes(amxc_var_t* priv) {
    if(GET_ARG(priv, AMXB_DEFERRED_CALLID) != NULL) {
        amxc_var_t* delvar_callid;
        uint64_t callid = GET_UINT64(priv, AMXB_DEFERRED_CALLID);
        SAH_TRACEZ_INFO(ME, "Have " AMXB_DEFERRED_CALLID " %" PRIu64 " so returning success on the rpc", callid);
        amxc_var_t deferred_data;
        amxc_var_init(&deferred_data);
        amxc_var_set_type(&deferred_data, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, &deferred_data, AMXB_DEFERRED_ERR_CODE, 0);
        amxc_var_add_key(cstring_t, &deferred_data, AMXB_DEFERRED_ERR_MSG, "");
        amxd_function_deferred_done(callid, amxd_status_ok, &deferred_data, NULL);
        amxc_var_clean(&deferred_data);
        delvar_callid = amxc_var_take_key(priv, AMXB_DEFERRED_CALLID);
        amxc_var_delete(&delvar_callid);
    }
}

static void cthulhu_notify_handler(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    timingila_command_entry_t* cmd = NULL;
    amxc_var_t* info = NULL;

    amxc_var_t* dm_obj_data = GET_ARG(data, CTHULHU_NOTIF_DM_OBJ_FULL);
    amxc_var_t* dm_obj_changed = GET_ARG(data, CTHULHU_NOTIF_DM_OBJ_CHANGED);
    const char* command_id = GET_CHAR(data, CTHULHU_NOTIF_COMMAND_ID);
    const char* notification = GET_CHAR(data, AMXM_NOTIFICATION);

    if(notification == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find notification field");
        return;
    }

    if(!string_is_empty(command_id)) {
        cmd = timingila_command_find(&cthulhu_command_llist, timingila_command_id_from_hex_str(command_id));
        if(cmd == NULL) {
            SAH_TRACEZ_INFO(ME, "There is no internal cmd structure to track the call");
        }
    }

    if(strcmp(notification, CTHULHU_NOTIF_ERROR) == 0) {
        const char* dm_path = GET_CHAR(data, "path");
        info = GET_ARG(data, CTHULHU_NOTIF_INFORMATION);
        const char* command = GET_CHAR(info, CTHULHU_CMD_COMMAND);
        uint32_t faulttype = GET_UINT32(info, CTHULHU_NOTIF_ERROR_TYPE);
        const char* faulttypestr = GET_CHAR(info, CTHULHU_NOTIF_ERROR_TYPESTR);
        const char* reason = GET_CHAR(info, CTHULHU_NOTIF_REASON);
        SAH_TRACEZ_NOTICE(ME, "Got " CTHULHU_NOTIF_ERROR " with { command = \"%s\" ; faulttype = %" PRIu32 "; faulttypestr = \"%s\"; reason = \"%s\"}", \
                          command, faulttype, faulttypestr, reason);
        if(cmd) {
            if(GET_ARG(cmd->priv, AMXB_DEFERRED_CALLID) != NULL) {
                uint64_t callid = GET_UINT64(cmd->priv, AMXB_DEFERRED_CALLID);
                end_deferred_call_on_err(callid, faulttype, faulttypestr, reason);
            }
            if(strcmp(dm_path, CTHULHU_DM_CONTAINER ".") == 0) {
                cthulhu_notif_error(cmd, command, faulttype, faulttypestr, reason);
                if(strcmp(command, CTHULHU_CMD_CTR_CREATE) == 0) {
                    // Try uninstalling the container
                    SAH_TRACEZ_ERROR(ME, "Got " CTHULHU_NOTIF_ERROR " and have a cmd, with " CTHULHU_CMD_CTR_CREATE  " --> uninstall du");
                    cthulhu_emit_signal(EVENT_EXEC_PACKAGER_UNINSTALL_DU, cmd->priv);
                } else if(strcmp(command, CTHULHU_CMD_CTR_UPDATE) == 0) {
                    const char* duid = NULL;
                    const char* version = NULL;
                    SAH_TRACEZ_ERROR(ME, "Got " CTHULHU_NOTIF_ERROR " and have a cmd, with " CTHULHU_CMD_CTR_UPDATE  " --> restore status to installed");
                    amxc_var_t* varptr = GET_ARG(dm_obj_data, SM_DM_DU_STATUS);
                    amxc_var_set(cstring_t, varptr, SM_DM_EVENT_DUSTATECHANGE_INSTALLED);
                    update_du_from_cthulhu_parsed_create(dm_obj_data);
                    version = GET_CHAR(cmd->priv, SM_DM_DU_VERSION);
                    duid = GET_CHAR(cmd->priv, SM_DM_DU_DUID);
                    if(version && duid) {
                        SAH_TRACEZ_ERROR(ME, "Remove new du again then (as it succeeded in rlyeh) (%s:%s)", duid, version);
                        emit_rlyeh_uninstall(duid, version);
                    } else {
                        SAH_TRACEZ_ERROR(ME, "Cannot remove the new du (%p:%p)", duid, version);
                    }
                } else if(strcmp(command, CTHULHU_CMD_CTR_REMOVE) == 0) {
                    SAH_TRACEZ_ERROR(ME, "Got " CTHULHU_NOTIF_ERROR " and have a cmd, with " CTHULHU_CMD_CTR_REMOVE  " --> uninstall du");
                    // let rlyeh remove the image anyway
                    cthulhu_emit_signal(EVENT_CONTAINER_UNINSTALL_DU_DONE, cmd->priv);
                } else {
                    SAH_TRACEZ_WARNING(ME, "Got " CTHULHU_NOTIF_ERROR " on " CTHULHU_DM_CONTAINER " and have a cmd, but don't have any action to perform (%s)", command);
                }
            }
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_CTR_CREATED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_CTR_CREATED);
        // Update SoftwareModules.DeploymentUnit instance
        if(cmd == NULL) {
            SAH_TRACEZ_ERROR(ME, "Need command");
            update_eu_from_cthulhu_parsed(dm_obj_data, NULL);
        } else {
            // update du
            // replace status
            amxc_var_t* varptr = GET_ARG(dm_obj_data, SM_DM_DU_STATUS);
            amxc_var_set(cstring_t, varptr, SM_DM_EVENT_DUSTATECHANGE_INSTALLED);
            update_du_from_cthulhu_parsed_create(dm_obj_data);

            // operation complete
            end_deferred_call_on_succes(cmd->priv);

            // Invoking update_eu_from_cthulhu_parsed() before event_dustatechange_from_cthulhu(),
            // so that EU details available in DuStateChange event.
            update_eu_from_cthulhu_parsed(dm_obj_data, NULL);
            // emit dustatechange
            amxc_var_add_key(cstring_t, cmd->priv, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL);
            event_dustatechange_from_cthulhu(cmd->priv);
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_CTR_REMOVED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_CTR_REMOVED);
        amxc_var_t* varptr = GET_ARG(dm_obj_data, SM_DM_DU_STATUS);
        if(cmd) {
            // Put status to uninstalling
            amxc_var_set(cstring_t, varptr, SM_DM_DU_STATUS_UNINSTALLING);
            update_du_from_cthulhu_parsed(dm_obj_data);
            // TODO: The modified status SM_DM_DU_STATUS_UNINSTALLING is not sent to the signal/slot
            // instead we are sending the old one, rlyeh will be modifying it again anyway
            // TODO: the format should be TR181 specific, right now I just copy the event parameters
            // to the signal
            cthulhu_emit_signal(EVENT_CONTAINER_UNINSTALL_DU_DONE, cmd->priv);
        }
        remove_eu_from_cthulhu_parsed(dm_obj_data);
    } else if(strcmp(notification, CTHULHU_NOTIF_CTR_UPDATED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_CTR_UPDATED);
        // update eu before changing status again
        update_eu_from_cthulhu_parsed(dm_obj_data, NULL);
        // replace status
        amxc_var_t* varptr = GET_ARG(dm_obj_data, SM_DM_DU_STATUS);
        amxc_var_set(cstring_t, varptr, SM_DM_EVENT_DUSTATECHANGE_INSTALLED);
        update_du_from_cthulhu_parsed(dm_obj_data);
        if(cmd) {
            const char* newversion = NULL;
            const char* oldversion = NULL;
            const char* duid = NULL;
            uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_CTHULHU_METHOD);
            // operation complete
            // both for eu_setrequestedstate and updatedu
            // operation complete
            end_deferred_call_on_succes(cmd->priv);
            if(method == TIMINGILA_CTHULHU_METHOD_UPDATEDU) {
                // DuStateChange only for updatedu case ;)
                // not necessary for eu_setrequestedstate
                amxc_var_add_key(cstring_t, cmd->priv, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UPDATE);
                event_dustatechange_from_cthulhu(cmd->priv);
            }
            oldversion = GET_CHAR(cmd->priv, SM_DM_DU_VERSION_OLD);
            duid = GET_CHAR(cmd->priv, SM_DM_DU_DUID);
            newversion = GET_CHAR(cmd->priv, SM_DM_DU_VERSION);
            if(oldversion && duid) {
                if(newversion) {
                    if(strcmp(oldversion, newversion) != 0) {
                        SAH_TRACEZ_INFO(ME, "Remove the old du in rlyeh (%s:%s)", duid, oldversion);
                        emit_rlyeh_uninstall(duid, oldversion);
                    } else {
                        SAH_TRACEZ_ERROR(ME, "Versions are the same [%s] ('%s' == '%s') we will not delete the old du in rlyeh", duid, oldversion, newversion);
                    }
                } else {
                    SAH_TRACEZ_WARNING(ME, "Cannot determine the new version, so we delete the old du without comparing new versus old version");
                    SAH_TRACEZ_INFO(ME, "Remove the old du in rlyeh (%s:%s)", duid, oldversion);
                    emit_rlyeh_uninstall(duid, oldversion);
                }
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot remove the old du in rlyeh (%p:%p)", duid, oldversion);
            }
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_SB_CREATED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_SB_CREATED);
        update_ee_from_cthulhu_parsed(dm_obj_data, dm_obj_changed);
        if(cmd) {
            // operation complete
            end_deferred_call_on_succes(cmd->priv);
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_SB_REMOVED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_SB_REMOVED);
        remove_ee_from_cthulhu_parsed(dm_obj_data);
        if(cmd) {
            // operation complete
            end_deferred_call_on_succes(cmd->priv);
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_SB_UPDATED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_SB_UPDATED);
        update_ee_from_cthulhu_parsed(dm_obj_data, dm_obj_changed);
        if(cmd) {
            // operation complete
            end_deferred_call_on_succes(cmd->priv);
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_SB_LIST) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_SB_LIST);
        amxc_var_t* sbinstancelist = GET_ARG(dm_obj_data, CTHULHU_DM_SANDBOX_INSTANCES);
        if(sbinstancelist) {
            amxc_var_for_each(sbinstance, sbinstancelist) {
                update_ee_from_cthulhu_parsed(sbinstance, NULL);
            }
        }
    } else if(strcmp(notification, CTHULHU_NOTIF_CTR_LIST) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_CTR_LIST);
        amxc_var_t* ctrinstancelist = GET_ARG(dm_obj_data, CTHULHU_DM_CONTAINER_INSTANCES);
        if(ctrinstancelist) {
            amxc_var_for_each(ctrinstance, ctrinstancelist) {
                update_eu_from_cthulhu_parsed(ctrinstance, NULL);
                if(lateinit) {
                    // TODO: Expecting this to break a lot :/ but all the keys are here
                    // Rlyeh can't provide the keys so we need cthulhu first
                    update_du_from_cthulhu_parsed_list(ctrinstance);
                }
            }

            if(lateinit) {
                // We emit the signal after we have the right structures to overlay ;)
                // Rlyeh doesnt have the required keys (alias and eeref)
                SAH_TRACEZ_WARNING(ME, "Too much ctr list notifications will malform the dm's, USE WITH CAUTION");
                cthulhu_emit_signal(EVENT_TIMINGILA_LATE_INIT_HANDOVER, NULL);
                lateinit = false;
            }
        }
        // TODO: put respective du's to installed ;)
    } else if(strcmp(notification, CTHULHU_NOTIF_INITIALIZED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_NOTIF_INITIALIZED);
        SAH_TRACEZ_INFO(ME, "Getting basic lxc info");
        cthulhu_get_info(NULL);
        SAH_TRACEZ_INFO(ME, "Sending CTHULHU_CMD_SB_LS: " CTHULHU_CMD_SB_LS);
        cthulhu_sandbox_command(CTHULHU_CMD_SB_LS, NULL, cthulhu_sb_list_done_cb, NULL);
    } else if(strcmp(notification, AMXB_APP_START) == 0) {
        SAH_TRACEZ_WARNING(ME, CTHULHU " is started again, the module will work again");
    } else if(strcmp(notification, AMXB_APP_STOP) == 0) {
        SAH_TRACEZ_ERROR(ME, CTHULHU " is stopped, the module will not work properly");
        cthulhu_subscribed = false;
        wait_and_connect_cthulhu();
    } else if(strcmp(notification, CTHULHU_NOTIF_CTR_VALIDATED) == 0) {
        SAH_TRACEZ_INFO(ME, CTHULHU_CMD_CTR_VALIDATE);
        if(cmd && cmd->priv) {
            uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_CTHULHU_METHOD);
            switch(method) {
            case TIMINGILA_CTHULHU_METHOD_VALIDATEINSTALL:
                cthulhu_emit_signal(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE, cmd->priv);
                break;
            case TIMINGILA_CTHULHU_METHOD_VALIDATEUPDATE:
                cthulhu_emit_signal(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE, cmd->priv);
                break;
            default:
                SAH_TRACEZ_ERROR(ME, "Method not supported for validation (%d)", method);
                break;
            }
        }
    }

    if(cmd) {
        SAH_TRACEZ_INFO(ME, "removing command: %s", command_id);
        timingila_command_remove(&cmd);
    }
}

static int subscribe_to_cthulhu_root(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Subscribe to: " CTHULHU);

    retval = amxb_subscribe(amxb_bus_ctx_cthulhu,
                            CTHULHU,
                            NULL,
                            cthulhu_notify_handler,
                            NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot subscribe to " CTHULHU " [%d]", retval);
    }

    return retval;
}

static int cthulhu_ee_setenable(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    int rc = -1;
    const char* name = NULL;
    bool enable = false;
    amxc_var_t privargs;
    amxc_var_t privret;
    char searchpath[SEARCHPATH_SIZE];

    when_null_log(args, exit);

    name = GET_CHAR(args, SM_DM_EE_NAME);
    if(name == NULL) {
        SAH_TRACEZ_ERROR(ME, "No " SM_DM_EE_NAME " given");
        goto exit;
    }

    rc = snprintf(searchpath, SEARCHPATH_SIZE, CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID "=='%s'].", name);
    if(rc < 1) {
        SAH_TRACEZ_ERROR(ME, "Couldnt construct searchpath");
        goto exit;
    }

    SAH_TRACEZ_ERROR(ME, "Searchpath: '%s'", searchpath);

    enable = GET_BOOL(args, SM_DM_EE_ENABLE);

    amxc_var_init(&privret);
    amxc_var_init(&privargs);
    amxc_var_set_type(&privargs, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &privargs, CTHULHU_SANDBOX_ENABLE, enable);

    rc = amxb_set(amxb_bus_ctx_cthulhu, searchpath, &privargs, &privret, TIMINGILA_CTHULHU_SYNC_TIMEOUT);
    amxc_var_clean(&privargs);
    amxc_var_clean(&privret);
exit:
    return rc;
}

static int cthulhu_ee_add(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    const char* sb_id = NULL;
    const char* cthulhu_command = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_ee_add()");

    // decode args
    sb_id = GET_CHAR(args, SM_DM_EE_NAME);
    when_null_log(sb_id, exit)

    cthulhu_command = CTHULHU_CMD_SB_CREATE;

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_ID, sb_id);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_PARENT, GET_CHAR(args, SM_DM_EE_PARENT_EE));
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_VERSION, GET_CHAR(args, SM_DM_EE_VERSION));
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_VENDOR, GET_CHAR(args, SM_DM_EE_VENDOR));
    amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_CPU, GET_INT32(args, SM_DM_EE_ALLOCCPUPERCENT));
    amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_MEMORY, GET_INT32(args, SM_DM_EE_ALLOCMEM));
    amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_DISKSPACE, GET_INT32(args, SM_DM_EE_ALLOCDISKSPACE));
    amxc_var_add_key(csv_string_t, cthulhu_args, CTHULHU_SANDBOX_AVAILABLE_ROLES, GET_CHAR(args, SM_DM_EE_AVAILABLE_ROLES));
    // Don't start EE directly
    amxc_var_add_key(bool, cthulhu_args, CTHULHU_SANDBOX_ENABLE, false);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EE_ADD);
    amxc_var_add_key(cstring_t, priv_args, SM_DM_DU_EE_REF, sb_id);

    rc = cthulhu_sandbox_command(cthulhu_command, cthulhu_args, cthulhu_sb_create_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }

    amxc_var_delete(&cthulhu_args);

exit:
    return rc;
}

static int cthulhu_ee_delete(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    const char* sb_id = NULL;
    const char* cthulhu_command = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_ee_delete()");

    // decode args
    sb_id = GET_CHAR(args, SM_DM_EE_NAME);
    if(sb_id == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA "missing " SM_DM_EE_NAME);
        goto exit;
    }

    cthulhu_command = CTHULHU_CMD_SB_REMOVE;

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_ID, sb_id);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EE_DELETE);
    amxc_var_add_key(cstring_t, priv_args, SM_DM_DU_EE_REF, sb_id);

    rc = cthulhu_sandbox_command(cthulhu_command, cthulhu_args, cthulhu_sb_delete_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }

    amxc_var_delete(&cthulhu_args);

exit:
    return rc;
}

static int cthulhu_ee_restart(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    const char* sb_id = NULL;
    const char* cthulhu_command = NULL;
    const char* restart_reason = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_ee_restart()");

    // decode args
    sb_id = GET_CHAR(args, SM_DM_EE_NAME);
    if(sb_id == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA "missing " SM_DM_EE_NAME);
        goto exit;
    }

    restart_reason = GET_CHAR(args, SM_DM_EE_RESTART_REASON);
    if(restart_reason == NULL) {
        SAH_TRACEZ_ERROR(ME, CTHULHU "missing " SM_DM_EE_RESTART_REASON);
        goto exit;
    }

    cthulhu_command = CTHULHU_CMD_SB_RESTART;

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_ID, sb_id);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_RESTART_REASON, restart_reason);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EE_RESTART);
    amxc_var_add_key(cstring_t, priv_args, SM_DM_DU_EE_REF, sb_id);

    rc = cthulhu_sandbox_command(cthulhu_command, cthulhu_args, cthulhu_sb_restart_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }

    amxc_var_delete(&cthulhu_args);

exit:
    return rc;
}

static int cthulhu_ee_modifyavailableroles(UNUSED const char* function_name,
                                           amxc_var_t* args,
                                           UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    amxc_var_t* available_roles = NULL;
    const char* sb_id = NULL;

    SAH_TRACEZ_INFO(ME, "cthulhu_ee_modifyavailableroles()");

    when_null_log(args, exit);

    // decode args
    sb_id = GET_CHAR(args, SM_DM_EE_NAME);
    when_null_log(sb_id, exit);

    available_roles = GET_ARG(args, SM_DM_EE_AVAILABLE_ROLES);
    when_null_log(available_roles, exit);

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_ID, sb_id);
    amxc_var_set_key(cthulhu_args, CTHULHU_SANDBOX_AVAILABLE_ROLES, available_roles, AMXC_VAR_FLAG_COPY);

    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EE_MODIFY);
    amxc_var_add_key(cstring_t, priv_args, SM_DM_DU_EE_REF, sb_id);

    rc = cthulhu_sandbox_command(CTHULHU_CMD_SB_MODIFY, cthulhu_args, cthulhu_sb_modify_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }

    amxc_var_delete(&cthulhu_args);
exit:
    return rc;
}

static int cthulhu_ee_modifyconstraints(UNUSED const char* function_name,
                                        amxc_var_t* args,
                                        UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    const char* sb_id = NULL;
    amxc_var_t* alloc_cpu_var = NULL;
    int32_t allocated_cpu_percent = -1;
    amxc_var_t* alloc_mem_var = NULL;
    int32_t allocated_memory = -1;
    amxc_var_t* alloc_diskspace_var = NULL;
    int32_t allocated_diskspace = -1;

    SAH_TRACEZ_INFO(ME, "cthulhu_ee_modifyconstraints()");

    when_null_log(args, exit);

    alloc_cpu_var = GET_ARG(args, SM_DM_EE_ALLOCCPUPERCENT);
    allocated_cpu_percent = GET_INT32(args, SM_DM_EE_ALLOCCPUPERCENT);
    alloc_mem_var = GET_ARG(args, SM_DM_EE_ALLOCMEM);
    allocated_memory = GET_INT32(args, SM_DM_EE_ALLOCMEM);
    alloc_diskspace_var = GET_ARG(args, SM_DM_EE_ALLOCDISKSPACE);
    allocated_diskspace = GET_INT32(args, SM_DM_EE_ALLOCDISKSPACE);

    // decode args
    sb_id = GET_CHAR(args, SM_DM_EE_NAME);
    when_null_log(args, exit);

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_SANDBOX_ID, sb_id);
    amxc_var_add_key(bool, cthulhu_args, CTHULHU_ARG_FORCE, GET_BOOL(args, SM_DM_EE_ARG_FORCE));
    if(alloc_cpu_var) {
        amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_CPU, allocated_cpu_percent);
    }
    if(alloc_mem_var) {
        amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_MEMORY, allocated_memory);
    }
    if(alloc_diskspace_var) {
        amxc_var_add_key(int32_t, cthulhu_args, CTHULHU_SANDBOX_DISKSPACE, allocated_diskspace);
    }

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EE_MODIFY);

    rc = cthulhu_sandbox_command(CTHULHU_CMD_SB_MODIFY, cthulhu_args, cthulhu_sb_modify_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }

    amxc_var_delete(&cthulhu_args);
exit:
    return rc;
}

static const char* map_ee_to_sb_resource(const char* ee_resource) {
    const char* sb_resource;

    if(strcmp(ee_resource, SM_DM_EE_AVAILDISKSPACE) == 0) {
        sb_resource = CTHULHU_STATS_DISKSPACE;
    } else if(strcmp(ee_resource, SM_DM_EE_AVAILMEM) == 0) {
        sb_resource = CTHULHU_STATS_MEMORY;
    } else {
        sb_resource = NULL;
    }

    return sb_resource;
}

static int cthulhu_ee_description(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    int rc = -1;
    const char* ee_alias = NULL;
    const char* description = NULL;
    char searchpath[SEARCHPATH_SIZE];
    amxc_var_t privargs;
    amxc_var_init(&privargs);

    when_null_log(args, exit);

    // get args
    ee_alias = GET_CHAR(args, SM_DM_EE_ALIAS);
    description = GET_CHAR(args, SM_DM_EE_DESCRIPTION);

    amxc_var_set_type(&privargs, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &privargs, CTHULHU_SANDBOX_DESCRIPTION, description);

    rc = snprintf(searchpath, SEARCHPATH_SIZE, CTHULHU "." CTHULHU_SANDBOX "." CTHULHU_INSTANCES ".[Alias == '%s'].", ee_alias);
    if(rc < 1) {
        SAH_TRACEZ_ERROR(ME, "Couldnt construct searchpath");
        goto exit;
    }

    rc = amxb_set(amxb_bus_ctx_cthulhu, searchpath, &privargs, ret, TIMINGILA_CTHULHU_SYNC_TIMEOUT);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "amxb_set failed");
    }
    amxc_var_clean(&privargs);
exit:
    return rc;
}

static int cthulhu_ee_stats(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int rc = -1;
    const char* ee_alias;
    const char* ee_stat_name;
    amxc_string_t ee_path;
    amxc_string_init(&ee_path, 0);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t value;
    amxc_var_init(&value);

    when_null_log(args, exit);

    // get args
    ee_alias = GET_CHAR(args, SM_DM_EE_ALIAS);
    ee_stat_name = map_ee_to_sb_resource(GET_CHAR(args, SM_EE_STAT_NAME));
    if(ee_stat_name == NULL) {
        SAH_TRACEZ_WARNING(ME, "Incorrect name to get in cthulhu_ee_stats() : %s", GET_CHAR(args, SM_EE_STAT_NAME));
        goto exit;
    }

    ctx = amxb_be_who_has(CTHULHU);
    when_null_log(ctx, exit);

    amxc_string_setf(&ee_path, CTHULHU "." CTHULHU_SANDBOX "." CTHULHU_INSTANCES ".[Alias == '%s']." CTHULHU_STATS ".%s." CTHULHU_STATS_ELEM_FREE, ee_alias, ee_stat_name);

    rc = amxb_get(ctx, amxc_string_get(&ee_path, 0), 0, &value, TIMINGILA_CTHULHU_GET_TIMEOUT);
    amxc_var_move(ret, amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(&value))));

    amxc_var_clean(&value);
    amxc_string_clean(&ee_path);
exit:
    return rc;
}

static const char* softwaremodules_stats[] = {
    "AllocatedMemory",
    "AllocatedDiskSpace",
    "AllocatedCPU",
    "AvailableMemory",
    "AvailableDiskSpace",
    "MemoryInUse",
    "DiskSpaceInUse"
};

static const char* cthulhu_stats_names[] = {
    CTHULHU_CONTAINER_RESOURCES_MEMORY,
    CTHULHU_CONTAINER_RESOURCES_DISKSPACE,
    CTHULHU_CONTAINER_RESOURCES_CPU,
    CTHULHU_STATS_MEMORY,
    CTHULHU_STATS_DISKSPACE,
    CTHULHU_STATS_MEMORY,
    CTHULHU_STATS_DISKSPACE
};

static const char* cthulhu_stats_types[] = {
    "",
    "",
    "",
    CTHULHU_STATS_ELEM_FREE,
    CTHULHU_STATS_ELEM_FREE,
    CTHULHU_STATS_ELEM_USED,
    CTHULHU_STATS_ELEM_USED
};

typedef enum _softwaremodules_stats_id {
    softwaremodules_stats_id_allocated_memory = 0,
    softwaremodules_stats_id_allocated_diskspace,
    softwaremodules_stats_id_allocated_cpu,
    softwaremodules_stats_id_available_memory,
    softwaremodules_stats_id_available_diskspace,
    softwaremodules_stats_id_in_use_memory,
    softwaremodules_stats_id_in_use_diskspace,
    softwaremodules_stats_id_last             // delimiter of enum
} softwaremodules_stats_id_t;

/**
 * @brief convett a string to a @ref softwaremodules_stats_id_t
 *
 * @param string
 * @return softwaremodules_stats_id_t
 */
static softwaremodules_stats_id_t softwaremodules_string_to_stats_id(const char* string) {
    int status_index = (int) softwaremodules_stats_id_allocated_memory;
    softwaremodules_stats_id_t status = softwaremodules_stats_id_allocated_memory;
    if(!string) {
        return softwaremodules_stats_id_last;
    }
    while(status < softwaremodules_stats_id_last) {
        if(strncmp(softwaremodules_stats[status], string,
                   strlen(softwaremodules_stats[status])) == 0) {
            return status;
        }
        status = (softwaremodules_stats_id_t)++ status_index;
    }
    return softwaremodules_stats_id_last;
}

static int cthulhu_eu_stats(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int rc = -1;
    const char* eu_alias;
    const char* eu_stat_name;
    amxc_string_t eu_path;
    amxc_string_init(&eu_path, 0);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t value;
    amxc_var_init(&value);

    when_null_log(args, exit);

    // get args
    eu_alias = GET_CHAR(args, SM_DM_EU_ALIAS);
    eu_stat_name = GET_CHAR(args, SM_EU_STAT_NAME);
    if(eu_stat_name == NULL) {
        SAH_TRACEZ_WARNING(ME, "Incorrect value to get in cthulhu_eu_stats() : %s", GET_CHAR(args, SM_EU_STAT_NAME));
        goto exit;
    }

    softwaremodules_stats_id_t statsid = softwaremodules_string_to_stats_id(eu_stat_name);
    if(statsid == softwaremodules_stats_id_last) {
        SAH_TRACEZ_ERROR(ME, "Invalid stat name %s", eu_stat_name);
        goto exit;
    }

    ctx = amxb_be_who_has(CTHULHU);
    when_null_log(ctx, exit);

    if(!string_is_empty(cthulhu_stats_types[statsid])) {
        amxc_string_setf(&eu_path, CTHULHU "." CTHULHU_CONTAINER "." CTHULHU_INSTANCES ".[Alias == '%s']." CTHULHU_CONTAINER_RESOURCES "." CTHULHU_STATS ".%s.%s", eu_alias, cthulhu_stats_names[statsid], cthulhu_stats_types[statsid]);
    } else {
        amxc_string_setf(&eu_path, CTHULHU "." CTHULHU_CONTAINER "." CTHULHU_INSTANCES ".[Alias == '%s']." CTHULHU_CONTAINER_RESOURCES ".%s", eu_alias, cthulhu_stats_names[statsid]);
    }

    rc = amxb_get(ctx, amxc_string_get(&eu_path, 0), 0, &value, TIMINGILA_CTHULHU_GET_TIMEOUT);
    amxc_var_move(ret, amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(&value))));

    amxc_var_clean(&value);
    amxc_string_clean(&eu_path);
exit:
    return rc;
}

static int cthulhu_eu_uptime(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int rc = -1;
    const char* eu_alias;
    amxc_string_t eu_path;
    amxc_string_init(&eu_path, 0);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t value;
    char* running_time = NULL;
    uint32_t up_time = 0;
    amxc_ts_t now;
    amxc_ts_t running_time_value;
    const char* status_value = NULL;

    amxc_var_init(&value);

    ASSERT_NOT_NULL(args, goto exit);

    // get args
    eu_alias = GET_CHAR(args, SM_DM_EU_ALIAS);
    status_value = GET_CHAR(args, SM_DM_EU_STATUS);
    if(strcmp(status_value, SM_DM_EU_STATUS_IDLE) == 0) {
        up_time = 0;
        rc = 0;
    } else if(strcmp(status_value, SM_DM_EU_STATUS_ACTIVE) == 0) {
        ctx = amxb_be_who_has(CTHULHU);
        ASSERT_NOT_NULL(ctx, goto exit);

        amxc_string_setf(&eu_path, CTHULHU "." CTHULHU_CONTAINER "." CTHULHU_INSTANCES ".[Alias == '%s']." CTHULHU_CONTAINER_STARTTIME, eu_alias);
        rc = amxb_get(ctx, amxc_string_get(&eu_path, 0), 0, &value, TIMINGILA_CTHULHU_GET_TIMEOUT);
        if(rc != 0) {
            SAH_TRACEZ_ERROR(ME, "amxb_get failed");
            goto exit;
        }

        running_time = amxc_var_get_cstring_t(amxc_var_get_first(amxc_var_get_first(amxc_var_get_first(&value))));

        // running_time_value will be parsed from the created_time_value /
        amxc_ts_parse(&running_time_value, running_time, LCM_CREATED_LEN);
        // current time will be parsed from &now /
        amxc_ts_now(&now);
        // calculate the uptime using the current time and running_time_value
        up_time = (int) now.sec - (int) running_time_value.sec;
    }

    amxc_var_set(uint32_t, ret, up_time);
    free(running_time);
exit:
    amxc_var_clean(&value);
    amxc_string_clean(&eu_path);
    return rc;
}

static int cthulhu_eu_setautostart(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    int rc = -1;
    const char* euid = NULL;
    bool autostart = false;
    amxc_var_t privargs;
    amxc_var_t privret;
    char searchpath[SEARCHPATH_SIZE];

    when_null_log(args, exit);

    euid = GET_CHAR(args, SM_DM_EU_EUID);
    when_null_log(euid, exit);

    rc = snprintf(searchpath, SEARCHPATH_SIZE, CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID "=='%s'].", euid);
    if(rc < 1) {
        SAH_TRACEZ_ERROR(ME, "Couldnt construct searchpath");
        goto exit;
    }

    autostart = GET_BOOL(args, SM_DM_EU_AUTOSTART);

    amxc_var_init(&privret);
    amxc_var_init(&privargs);
    amxc_var_set_type(&privargs, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &privargs, CTHULHU_CONTAINER_AUTOSTART, autostart);

    rc = amxb_set(amxb_bus_ctx_cthulhu, searchpath, &privargs, &privret, TIMINGILA_CTHULHU_SYNC_TIMEOUT);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "amxb_set failed");
    }
    amxc_var_clean(&privargs);
    amxc_var_clean(&privret);
exit:
    return rc;
}

static int cthulhu_eu_setreqstate(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;
    const char* ctrid = NULL;
    const char* cthulhu_command = NULL;
    const char* requested_state = NULL;
    const char* cur_state = NULL;
    const char* update_to_status = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_eu_setreqstate()");

    // decode args
    ctrid = GET_CHAR(args, SM_DM_EU_EUID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA " missing " SM_DM_EU_EUID);
        goto exit;
    }

    requested_state = GET_CHAR(args, SM_DM_EU_REQSTATE);
    if(requested_state == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA " missing " SM_DM_EU_REQSTATE);
        goto exit;
    }

    cur_state = GET_CHAR(args, SM_DM_EU_STATUS);
    if(requested_state == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA " missing " SM_DM_EU_STATUS);
        goto exit;
    }

    if(strcmp(cur_state, requested_state) == 0) {
        SAH_TRACEZ_ERROR(ME, "Container already in this state: %s == %s", cur_state, requested_state);
        goto exit;
    }

    if(strcmp(requested_state, SM_DM_EU_STATUS_ACTIVE) == 0) {
        cthulhu_command = CTHULHU_CMD_CTR_START;
        update_to_status = SM_DM_EU_STATUS_STARTING;
    } else if(strcmp(requested_state, SM_DM_EU_STATUS_IDLE) == 0) {
        cthulhu_command = CTHULHU_CMD_CTR_STOP;
        update_to_status = SM_DM_EU_STATUS_STOPPING;
    } else if(strcmp(requested_state, SM_DM_EU_STATUS_RESTART) == 0) {
        cthulhu_command = CTHULHU_CMD_CTR_RESTART;
        update_to_status = SM_DM_EU_STATUS_RESTARTING;
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid state requested: %s", requested_state);
        goto exit;
    }

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_CONTAINER_ID, ctrid);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_EU_SETREQSTATE);

    rc = cthulhu_container_command(cthulhu_command, cthulhu_args, cthulhu_ctr_rm_done_cb, (void*) priv_args);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "cthulhu_container_command (%s) failed (%d) so not updating " \
                         SM_DM_EU_EUID " (%s) to " SM_DM_EU_STATUS " (%s)", \
                         cthulhu_command, \
                         rc, \
                         ctrid, \
                         update_to_status);
        amxc_var_delete(&priv_args);
    } else {
        amxc_var_t* update_eu_args = NULL;
        SAH_TRACEZ_INFO(ME, "Updating " SM_DM_EU_EUID " (%s) to " SM_DM_EU_STATUS " (%s)", ctrid, update_to_status);
        amxc_var_new(&update_eu_args);
        amxc_var_set_type(update_eu_args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, update_eu_args, SM_DM_EU_EUID, ctrid);
        amxc_var_add_key(cstring_t, update_eu_args, SM_DM_EU_STATUS, update_to_status);
        update_eu_from_cthulhu(update_eu_args);
        amxc_var_clean(update_eu_args);
    }

exit:
    return rc;
}

static int cthulhu_eu_modifynetworkconfig(UNUSED const char* function_name,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* nc = NULL;
    const char* ctrid = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_eu_modifynetworkconfig()");

    // decode args
    ctrid = GET_CHAR(args, SM_DM_EU_EUID);
    if(ctrid == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA "missing " SM_DM_EU_EUID);
        goto exit;
    }

    nc = GET_ARG(args, SM_DM_EU_NC);
    if(nc == NULL) {
        SAH_TRACEZ_ERROR(ME, TIMINGILA "missing " SM_DM_EU_NC);
        goto exit;
    }

    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_CONTAINER_ID, ctrid);
    amxc_var_t* new_nc = amxc_var_add_new_key(cthulhu_args, CTHULHU_CONTAINER_NETWORKCONFIG);
    amxc_var_copy(new_nc, nc);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_MODIFYNETWORKCONFIG, cthulhu_args, cthulhu_empty_done_cb, NULL);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "cthulhu_container_command (" CTHULHU_CMD_CTR_MODIFYNETWORKCONFIG ") failed (%d)", rc);
    }
    amxc_var_delete(&cthulhu_args);

exit:
    amxc_var_set(bool, ret, (rc == amxd_status_ok));
    return rc;
}

static int cthulhu_ctr_remove(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    int rc = -1;
    const char* duid = NULL;
    bool retaindata = false;
    bool removedata = !retaindata;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv_args = NULL;

    when_null_log(args, exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_ctr_remove()");

    // decode args
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, CTHULHU "missing " SM_DM_DU_DUID);
        goto exit;
    }

    retaindata = GET_BOOL(args, SM_DM_DU_RPC_RETAINDATA);
    // invert the boolean for cthulhu
    removedata = !retaindata;

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, cthulhu_args, CTHULHU_CONTAINER_ID, duid);
    amxc_var_add_key(bool, cthulhu_args, CTHULHU_CONTAINER_REMOVEDATA, removedata);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_UNINSTALLDU);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_REMOVE, cthulhu_args, cthulhu_ctr_rm_done_cb, (void*) priv_args);
    if(rc) {
        amxc_var_delete(&priv_args);
    }
    amxc_var_delete(&cthulhu_args);
exit:
    return rc;
}

static inline void cthulhu_add_optional_parameter(const amxc_var_t* const src_args, amxc_var_t* const dest_args, const char* sm_key, const char* cthulhu_key) {
    amxc_var_t* var = NULL;
    if(( var = GET_ARG(src_args, sm_key)) != NULL) {
        amxc_var_set_key(dest_args, cthulhu_key, var, AMXC_VAR_FLAG_COPY);
    }
    if(( var = amxc_var_get_key(src_args, SM_DM_EU_AUTORESTART, AMXC_VAR_FLAG_DEFAULT)) != NULL) {
        amxc_var_set_key(dest_args, CTHULHU_CONTAINER_AUTORESTART, var, AMXC_VAR_FLAG_DEFAULT);
    }
}

static inline int cthulhu_add_mandatory_parameter(const amxc_var_t* const src_args, amxc_var_t* const dest_args, const char* sm_key, const char* cthulhu_key) {
    int rc = -1;
    amxc_var_t* var = NULL;
    var = GET_ARG(src_args, sm_key);
    if(!var) {
        SAH_TRACEZ_ERROR(ME, "Argument [%s] is not preset", sm_key);
        goto exit;
    }
    rc = amxc_var_set_key(dest_args, cthulhu_key, var, AMXC_VAR_FLAG_COPY);
exit:
    return rc;
}

static void cthulhu_add_optional_parameters(const amxc_var_t* const src_args, amxc_var_t* const dest_args) {
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_DISKLOCATION, CTHULHU_CONTAINER_DISKLOCATION);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_ALLOCCPUPERCENT, CTHULHU_CONTAINER_RESOURCES_CPU);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_ALLOCMEM, CTHULHU_CONTAINER_RESOURCES_MEMORY);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_ALLOCDISKSPACE, CTHULHU_CONTAINER_RESOURCES_DISKSPACE);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_AUTOSTART, CTHULHU_CONTAINER_AUTOSTART);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_HOSTOBJECT, CTHULHU_CONTAINER_HOSTOBJECT);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_NETWORKCONFIG, CTHULHU_CONTAINER_NETWORKCONFIG);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_DATA, CTHULHU_CONTAINER_DATA);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_MODULEVERSION, CTHULHU_CONTAINER_MODULEVERSION);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_ENVIRONMENTVARIABLES, CTHULHU_CONTAINER_ENVIRONMENTVARIABLES);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_REQUIRED_ROLES, CTHULHU_CONTAINER_REQUIRED_ROLES);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_OPTIONAL_ROLES, CTHULHU_CONTAINER_OPTIONAL_ROLES);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_PRIVILEGED, CTHULHU_CONTAINER_UNPRIVILEGED_PRIVILEGED);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_NUMREQUIREDUIDS, CTHULHU_CONTAINER_UNPRIVILEGED_NUMREQUIREDUIDS);
    cthulhu_add_optional_parameter(src_args, dest_args, SM_DM_DU_APPLICATIONDATA, CTHULHU_CONTAINER_APPLICATIONDATA);
}

static int cthulhu_update(const amxc_var_t* const args) {
    int rc = -1;
    bool retaindata = false;
    bool removedata = !retaindata;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv = NULL;

    SAH_TRACEZ_INFO(ME, CTHULHU_DM_CONTAINER "." CTHULHU_CMD_CTR_UPDATE "()");

    amxc_var_new(&cthulhu_args);
    ASSERT_NOT_NULL(args, goto exit);

    // translate args to cthulhu
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);

    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_DUID, CTHULHU_CONTAINER_ID), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_NAME, CTHULHU_CONTAINER_BUNDLE), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_VERSION, CTHULHU_CONTAINER_BUNDLEVERSION), goto exit);
    // add optional parameters
    cthulhu_add_optional_parameters(args, cthulhu_args);
    // invert the boolean for cthulhu
    retaindata = GET_BOOL(args, SM_DM_DU_RPC_RETAINDATA);
    removedata = !retaindata;
    amxc_var_add_key(bool, cthulhu_args, CTHULHU_CONTAINER_REMOVEDATA, removedata);

    // create priv
    amxc_var_new(&priv);
    if(amxc_var_copy(priv, args)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't copy priv args");
        amxc_var_delete(&priv);
        goto exit;
    }
    amxc_var_set_uint32_t(GET_ARG(priv, TIMINGILA_CTHULHU_METHOD), TIMINGILA_CTHULHU_METHOD_UPDATEDU);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_UPDATE, cthulhu_args, cthulhu_ctr_update_done_cb, priv);
    if(rc) {
        amxc_var_delete(&priv);
    }
exit:
    amxc_var_delete(&cthulhu_args);
    return rc;
}

static int cthulhu_eu_validateupdateargs(UNUSED const char* function_name,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv = NULL;

    ASSERT_NOT_NULL(args, goto exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_eu_validateinstallargs()");

    // translate args to cthulhu
    amxc_var_new(&cthulhu_args);
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);

    cthulhu_add_optional_parameter(args, cthulhu_args, SM_DM_DU_DUID, CTHULHU_CONTAINER_ID);
    cthulhu_add_optional_parameter(args, cthulhu_args, SM_DM_DU_VERSION, CTHULHU_CONTAINER_BUNDLEVERSION);

    // add optional parameters
    cthulhu_add_optional_parameters(args, cthulhu_args);

    // create priv
    amxc_var_new(&priv);
    if(amxc_var_copy(priv, args)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't copy priv args");
        amxc_var_delete(&priv);
        goto exit;
    }
    amxc_var_add_key(uint32_t, priv, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_VALIDATEUPDATE);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_VALIDATE, cthulhu_args, cthulhu_ctr_validate_update_done_cb, priv);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "cthulhu_container_command (" CTHULHU_CMD_CTR_VALIDATE ") failed (%d)", rc);
    }
exit:
    amxc_var_delete(&cthulhu_args);
    amxc_var_set(bool, ret, (rc == amxd_status_ok));
    return rc;
}

static int cthulhu_create(const amxc_var_t* const args) {
    int rc = -1;
    const char* executionenvref = NULL;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv = NULL;

    SAH_TRACEZ_INFO(ME, CTHULHU_DM_CONTAINER "." CTHULHU_CMD_CTR_CREATE "()");

    amxc_var_new(&cthulhu_args);
    ASSERT_NOT_NULL(args, goto exit);
    executionenvref = GET_CHAR(args, SM_DM_DU_EE_REF);

    // translate args to cthulhu
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);

    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_DUID, CTHULHU_CONTAINER_ID), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_NAME, CTHULHU_CONTAINER_BUNDLE), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_VERSION, CTHULHU_CONTAINER_BUNDLEVERSION), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_EE_REF, CTHULHU_CONTAINER_SANDBOX), goto exit);
    ASSERT_SUCCESS(cthulhu_add_mandatory_parameter(args, cthulhu_args, SM_DM_DU_UUID, CTHULHU_CONTAINER_LINKED_UUID), goto exit);

    // add optional parameters
    cthulhu_add_optional_parameters(args, cthulhu_args);

    // create priv
    amxc_var_new(&priv);
    if(amxc_var_copy(priv, args)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't copy priv args");
        amxc_var_delete(&priv);
        goto exit;

    }
    amxc_var_set_uint32_t(GET_ARG(priv, TIMINGILA_CTHULHU_METHOD), TIMINGILA_CTHULHU_METHOD_CREATEDU);
    amxc_var_add_key(cstring_t, priv, SM_DM_DU_EE_REF, executionenvref);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_CREATE, cthulhu_args, cthulhu_ctr_create_done_cb, priv);
    if(rc) {
        amxc_var_delete(&priv);
    }
exit:
    amxc_var_delete(&cthulhu_args);
    return rc;
}

static int cthulhu_eu_validateinstallargs(UNUSED const char* function_name,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* cthulhu_args = NULL;
    amxc_var_t* priv = NULL;

    amxc_var_new(&cthulhu_args);
    ASSERT_NOT_NULL(args, goto exit);

    SAH_TRACEZ_INFO(ME, "cthulhu_eu_validateinstallargs()");

    // translate args to cthulhu
    amxc_var_set_type(cthulhu_args, AMXC_VAR_ID_HTABLE);

    cthulhu_add_optional_parameter(args, cthulhu_args, SM_DM_DU_DUID, CTHULHU_CONTAINER_ID);
    cthulhu_add_optional_parameter(args, cthulhu_args, SM_DM_DU_EE_REF, CTHULHU_CONTAINER_SANDBOX);
    cthulhu_add_optional_parameter(args, cthulhu_args, SM_DM_DU_UUID, CTHULHU_CONTAINER_LINKED_UUID);

    // add optional parameters
    cthulhu_add_optional_parameters(args, cthulhu_args);

    // create priv
    amxc_var_new(&priv);
    if(amxc_var_copy(priv, args)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't copy priv args");
        amxc_var_delete(&priv);
        goto exit;
    }
    amxc_var_add_key(uint32_t, priv, TIMINGILA_CTHULHU_METHOD, TIMINGILA_CTHULHU_METHOD_VALIDATEINSTALL);

    rc = cthulhu_container_command(CTHULHU_CMD_CTR_VALIDATE, cthulhu_args, cthulhu_ctr_validate_install_done_cb, priv);
    if(rc) {
        SAH_TRACEZ_ERROR(ME, "cthulhu_container_command (" CTHULHU_CMD_CTR_VALIDATE ") failed (%d)", rc);
    }

exit:
    amxc_var_delete(&cthulhu_args);
    amxc_var_set(bool, ret, (rc == amxd_status_ok));
    return rc;
}

static void init_command_id_counter(void) {
    SAH_TRACEZ_INFO(ME, "Intialize command id counter with random");
    cthulhu_command_id_counter = timingila_init_command_id_counter();
    SAH_TRACEZ_INFO(ME, "cthulhu_command_id_counter = %X", cthulhu_command_id_counter);
    return;
}

static void slot_packager_install_du_done(const char* const sig_name,
                                          const amxc_var_t* const data,
                                          UNUSED void* priv) {
    int ret;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_ERROR(ME, "No data");
        return;
    }

    ret = cthulhu_create(data);
    if(ret) {
        const char* duid = GET_CHAR(data, SM_DM_DU_DUID);
        const char* uuid = GET_CHAR(data, SM_DM_DU_UUID);
        const char* version = GET_CHAR(data, SM_DM_DU_VERSION);
        uint64_t timingila_callid = GET_UINT64(data, AMXB_DEFERRED_CALLID);
        amxc_var_t notif_data;

        if((uuid != NULL) && (uuid[0] != '\0')) {
            SAH_TRACEZ_ERROR(ME, "Issue dustatechange event");
            amxc_var_init(&notif_data);
            amxc_var_set_type(&notif_data, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
            amxc_var_add_key(uint32_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, ERR_USP_INTERNAL_ERROR);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, ERR_INTERNAL_ERR_PREFIX ERR_CTHULHU_CONTAINER_CREATE_STRING);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL);
            event_dustatechange_from_cthulhu(&notif_data);
            amxc_var_clean(&notif_data);
        } else {
            SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_UUID);
        }

        if((version != NULL) && (version[0] != '\0')) {
            if((duid != NULL) && (duid[0] != '\0')) {
                SAH_TRACEZ_ERROR(ME, "Putting entry to failed (%s:%s)", duid, version);
                update_du_status(duid, version, SM_DM_DU_STATUS_FAILED);
                SAH_TRACEZ_ERROR(ME, "Deleting entry (%s:%s)", duid, version);
                emit_rlyeh_uninstall(duid, version);
            } else {
                SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_DUID);
            }
        } else {
            SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_VERSION);
        }

        end_deferred_call_on_err(timingila_callid, ERR_USP_INTERNAL_ERROR, ERR_INTERNAL_ERR_PREFIX ERR_CTHULHU_CONTAINER_CREATE_STRING, NULL);
    }
}

static int update_networkconfig_from_cthulhu(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    when_null_log(data, exit);

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_NC, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_NC ") return code %d",
                    amxm_ret);
    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

/**
 * @brief convert result of amxb_get to table
 *
 * amxb_get does not return a table. but a list of keys. This function will compact this result to a table
 *
 * {
 *      Cthulhu.Plugins.NetworkConfig.Interfaces. = {
 *      },
 *      Cthulhu.Plugins.NetworkConfig.Interfaces.1. = {
 *          Name = "Wan",
 *          Reference = "Device.Logical.Interface.1."
 *      },
 *      Cthulhu.Plugins.NetworkConfig.Interfaces.2. = {
 *          Name = "Lan",
 *          Reference = "Device.Logical.Interface.2."
 *      }
 * }
 *  To
 * {
 *      Cthulhu= {
 *          Plugins = {
 *              NetworkConfig = {
 *                  Interfaces = {
 *                      "1" = {
 *                          "Name" = "Wan",
 *                          "Reference" = "Device.Logical.Interface.1."
 *                      },
 *                      "2" = {
 *                          "Name" = "Lan",
 *                          "Reference" = "Device.Logical.Interface.2."
 *                      }
 *                  }
 *              }
 *      }
 * }
 * }
 * @param args
 * @return amxc_var_t*
 */
static amxc_var_t* compact_args_to_table(amxc_var_t* args) {
    amxc_var_t* table = NULL;
    amxc_var_new(&table);
    when_null(table, exit);
    amxc_var_set_type(table, AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(arg, args) {
        const char* key = amxc_var_key(arg);
        amxc_var_t* arg_table = amxc_var_get_path(table, key, AMXC_VAR_FLAG_DEFAULT);
        if(!arg_table) {
            // create the table
            amxc_llist_t list;
            amxc_llist_init(&list);
            amxc_string_t key_str;
            amxc_string_init(&key_str, 64);
            amxc_string_appendf(&key_str, "%s", key);

            if(amxc_string_split_to_llist(&key_str, &list, '.') != AMXC_STRING_SPLIT_OK) {
                SAH_TRACEZ_ERROR(ME, "Could not split %s", key);
                amxc_llist_clean(&list, amxc_string_list_it_free);
                amxc_string_clean(&key_str);
                goto exit_error;
            }
            amxc_var_t* last_elem = table;
            amxc_llist_for_each(it, &list) {
                amxc_string_t* s = amxc_string_from_llist_it(it);
                if(!s || string_is_empty(s->buffer)) {
                    continue;
                }
                amxc_var_t* this_elem = GET_ARG(last_elem, s->buffer);
                if(!this_elem) {
                    this_elem = amxc_var_add_new_key_amxc_htable_t(last_elem, s->buffer, NULL);
                    if(!this_elem) {
                        SAH_TRACEZ_ERROR(ME, "Could not create %s. failed on %s", key, s->buffer);
                        amxc_llist_clean(&list, amxc_string_list_it_free);
                        amxc_string_clean(&key_str);
                        goto exit_error;
                    }
                }
                last_elem = this_elem;
            }
            amxc_llist_clean(&list, amxc_string_list_it_free);
            amxc_string_clean(&key_str);
            arg_table = last_elem;
        }
        // copy the data of the arg into the new table
        amxc_var_for_each(param, arg) {
            const char* param_key = amxc_var_key(param);
            amxc_var_t* new = GET_ARG(arg_table, param_key);
            if(!new) {
                new = amxc_var_add_new_key(arg_table, param_key);
            }
            amxc_var_copy(new, param);
        }
    }
    goto exit;
exit_error:
    amxc_var_delete(&table);
exit:
    return table;

}

static int cthulhu_get_networkconfig(void) {
    int ret = -1;
    amxc_var_t networkconfig;
    amxc_var_t args;
    amxc_var_t* compact_table = NULL;
    amxc_var_init(&networkconfig);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Getting NetworkConfig from cthulhu");

    ret = amxb_get(amxb_bus_ctx_cthulhu, CTHULHU_DM_NETWORKCONFIG ".", 5, &networkconfig, TIMINGILA_CTHULHU_SYNC_TIMEOUT);
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Could not get " CTHULHU_DM_NETWORKCONFIG ".");
        goto exit;
    }
    amxc_var_t* first = amxc_var_get_first(&networkconfig);
    when_null(first, exit);
    compact_table = compact_args_to_table(first);
    amxc_var_t* nc = amxc_var_get_path(compact_table, CTHULHU_DM_NETWORKCONFIG, AMXC_VAR_FLAG_DEFAULT);
    when_null(nc, exit);
    copy_var_from_table(nc, &args, SM_DM_NC_DEFAULTBRIDGE);
    copy_var_from_table(nc, &args, SM_DM_NC_DEFAULTFIREWALLCHAIN);
    amxc_var_t* itfs = GET_ARG(nc, SM_DM_NC_INTERFACES);
    if(itfs) {
        amxc_var_t* itfs_new = amxc_var_add_new_key_amxc_htable_t(&args, SM_DM_NC_INTERFACES, NULL);
        amxc_var_for_each(itf, itfs) {
            const char* key = amxc_var_key(itf);
            if(!string_is_empty(key)) {
                amxc_var_t* itf_new = amxc_var_add_new_key_amxc_htable_t(itfs_new, key, NULL);
                copy_var_from_table(itf, itf_new, SM_DM_NC_INTERFACES_NAME);
                copy_var_from_table(itf, itf_new, SM_DM_NC_INTERFACES_REFERENCE);
            }
        }
    }
    ret = update_networkconfig_from_cthulhu(&args);

exit:
    amxc_var_clean(&networkconfig);
    amxc_var_clean(&args);
    amxc_var_delete(&compact_table);

    return ret;
}

static void slot_packager_update_du_done(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* priv) {
    int ret;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    ret = cthulhu_update(data);
    if(ret) {
        const char* duid = GET_CHAR(data, SM_DM_DU_DUID);
        const char* uuid = GET_CHAR(data, SM_DM_DU_UUID);
        const char* version = GET_CHAR(data, SM_DM_DU_VERSION);
        uint64_t timingila_callid = GET_UINT64(data, AMXB_DEFERRED_CALLID);
        amxc_var_t notif_data;

        if((uuid != NULL) && (uuid[0] != '\0')) {
            SAH_TRACEZ_ERROR(ME, "Issue dustatechange event");
            amxc_var_init(&notif_data);
            amxc_var_set_type(&notif_data, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
            amxc_var_add_key(uint32_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, ERR_USP_INTERNAL_ERROR);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, ERR_INTERNAL_ERR_PREFIX ERR_CTHULHU_CONTAINER_CREATE_STRING);
            amxc_var_add_key(cstring_t, &notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UPDATE);
            event_dustatechange_from_cthulhu(&notif_data);
            amxc_var_clean(&notif_data);
        } else {
            SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_UUID);
        }

        if((version != NULL) && (version[0] != '\0')) {
            if((duid != NULL) && (duid[0] != '\0')) {
                SAH_TRACEZ_ERROR(ME, "Putting entry to failed (%s:%s)", duid, version);
                update_du_status(duid, version, SM_DM_DU_STATUS_FAILED);
                SAH_TRACEZ_ERROR(ME, "Deleting entry (%s:%s)", duid, version);
                emit_rlyeh_uninstall(duid, version);
            } else {
                SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_DUID);
            }
        } else {
            SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_VERSION);
        }

        SAH_TRACEZ_ERROR(ME, "End deferred call");
        end_deferred_call_on_err(timingila_callid, ERR_USP_INTERNAL_ERROR, ERR_INTERNAL_ERR_PREFIX ERR_CTHULHU_CONTAINER_CREATE_STRING, NULL);
    }
}


static int cthulhu_ee_readparam(UNUSED const char* function_name,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    int rc = -1;
    const char* ee_alias;
    const char* relpath;
    const char* param_name;
    amxc_string_t eu_path;
    amxc_string_init(&eu_path, 0);
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t value;
    amxc_var_init(&value);

    ASSERT_NOT_NULL(args, goto exit);

    // get args
    ee_alias = GET_CHAR(args, SM_DM_EE_ALIAS);
    ASSERT_STR_NOT_EMPTY(ee_alias, goto exit);
    relpath = GET_CHAR(args, SM_EE_RELPATH);
    param_name = GET_CHAR(args, SM_EE_PARAM);
    ASSERT_STR_NOT_EMPTY(param_name, goto exit);

    ctx = amxb_be_who_has(CTHULHU);
    ASSERT_NOT_NULL(ctx, goto exit);

    if(STRING_EMPTY(relpath)) {
        amxc_string_setf(&eu_path, CTHULHU "." CTHULHU_SANDBOX "." CTHULHU_INSTANCES ".[Alias == '%s'].%s", ee_alias, param_name);
    } else {
        amxc_string_setf(&eu_path, CTHULHU "." CTHULHU_SANDBOX "." CTHULHU_INSTANCES ".[Alias == '%s'].%s.%s", ee_alias, relpath, param_name);

    }

    rc = amxb_get(ctx, amxc_string_get(&eu_path, 0), 0, &value, TIMINGILA_CTHULHU_GET_TIMEOUT);
    amxc_var_t* val = amxc_var_get_first(&value);
    while(amxc_var_type_of(val) == AMXC_VAR_ID_HTABLE ||
          amxc_var_type_of(val) == AMXC_VAR_ID_LIST) {
        val = amxc_var_get_first(val);
    }
    amxc_var_move(ret, val);

    amxc_var_clean(&value);
    amxc_string_clean(&eu_path);
exit:
    return rc;
}

static int add_slot(const char* const signal_name,
                    const char* const expression,
                    amxp_slot_fn_t fn,
                    void* const priv) {
    int retval = -255;
    if(string_is_empty(signal_name)) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        goto exit;
    }

    when_null_log(fn, exit);

    SAH_TRACEZ_INFO(ME, "Adding slot: %s [%p]",
                    signal_name,
                    fn);

    retval = timingila_add_slot_to_signal(signal_name, expression, fn, priv);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Error adding slot [%d]", retval);
    }
exit:
    return retval;
}

static void slot_wait_connect_cthulhu(UNUSED const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    wait_and_connect_cthulhu();
}

static void slot_cthulhu_up(const char* const sig_name,
                            UNUSED const amxc_var_t* const data,
                            UNUSED void* priv) {
    bool initialized = false;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);

    if(update_amxb_bus_ctx_cthulhu() < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not initialize Cthulhu bus context");
        goto exit;
    }
    if(subscribe_to_cthulhu_root()) {
        SAH_TRACEZ_ERROR(ME, "Could not subscribe to Cthulhu yet");
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Getting basic lxc info");
    if(cthulhu_get_info(&initialized)) {
        SAH_TRACEZ_ERROR(ME, "Could not get info on cthulhu");
        goto exit;
    }
    cthulhu_get_networkconfig();

    remove_slot(AMXB_WAIT_DONE, slot_cthulhu_up);


    if(initialized) {
        SAH_TRACEZ_WARNING(ME, "Cthulhu already initialized probe it asap");
        // Cthulhu is already initialized so dont wait on the event anymore
        SAH_TRACEZ_INFO(ME, "Sending CTHULHU_CMD_SB_LS: " CTHULHU_CMD_SB_LS);
        cthulhu_sandbox_command(CTHULHU_CMD_SB_LS, NULL, cthulhu_sb_list_done_cb, NULL);
    } else {
        SAH_TRACEZ_WARNING(ME, "Cthulhu not initialized wait for: " CTHULHU_NOTIF_INITIALIZED);
    }

    return;
exit:
    amxp_sigmngr_deferred_call(NULL, slot_wait_connect_cthulhu, NULL, NULL);
    return;
}

static void wait_and_connect_cthulhu(void) {
    SAH_TRACEZ_INFO(ME, "Waiting on " CTHULHU_DM ".");
    amxb_wait_for_object(CTHULHU_DM ".");
    add_slot(AMXB_WAIT_DONE, NULL, slot_cthulhu_up, NULL);
}


static int remove_slot(const char* const signal_name,
                       amxp_slot_fn_t fn) {
    int retval = -255;
    SAH_TRACEZ_INFO(ME, "Removing slot: %s [%p]", signal_name, fn);
    if(string_is_empty(signal_name)) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        goto exit;
    }

    when_null_log(fn, exit);

    retval = timingila_remove_slot_from_signal(signal_name, fn);
exit:
    return retval;
}

static void init_cthulhu_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Initialize cthulhu_command_llist");
    if(timingila_command_llist_init(&cthulhu_command_llist)) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize cthulhu_command_llist");
        return;
    }
}

static void cleanup_cthulhu_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Cleanup cthulhu_command_llist");
    timingila_command_llist_cleanup(&cthulhu_command_llist);
    return;
}

static void init_slots(void) {
    SAH_TRACEZ_INFO(ME, "Initializing slots");
    add_slot(EVENT_PACKAGER_INSTALL_DU_DONE, NULL, slot_packager_install_du_done, NULL);
    add_slot(EVENT_PACKAGER_UPDATE_DU_DONE, NULL, slot_packager_update_du_done, NULL);
    return;
}

static void remove_slots(void) {
    SAH_TRACEZ_INFO(ME, "Removing slots");
    remove_slot(EVENT_PACKAGER_INSTALL_DU_DONE, slot_packager_install_du_done);
    remove_slot(EVENT_PACKAGER_UPDATE_DU_DONE, slot_packager_update_du_done);
    remove_slot(AMXB_WAIT_DONE, slot_cthulhu_up);
    return;
}

static int cthulhu_adapter_init(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* var_dm = NULL;
    var_dm = GET_ARG(args, TIMINGILA_CONTAINER_INIT_DM);
    if(var_dm && var_dm->data.data) {
        dm = (amxd_dm_t*) var_dm->data.data;
    }
    if(!dm) {
        SAH_TRACEZ_ERROR(ME, "DM argument is not present");
        goto exit;
    }

    rc = 0;
exit:
    amxc_var_set(bool, ret, rc);
    return rc;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-cthulhu constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    when_null_log(so, exit);

    if(amxm_module_register(&mod, so, MOD_TIMINGILA_CONTAINER)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", MOD_TIMINGILA_CONTAINER);
        goto exit;
    }

    init_cthulhu_command_llist();
    init_command_id_counter();
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_UNINSTALLDU, cthulhu_ctr_remove);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_SETREQUESTEDSTATE, cthulhu_eu_setreqstate);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_SETAUTOSTART, cthulhu_eu_setautostart);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_MODIFYNETWORKCONFIG, cthulhu_eu_modifynetworkconfig);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_VALIDATEINSTALLARGS, cthulhu_eu_validateinstallargs);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_VALIDATEUPDATEARGS, cthulhu_eu_validateupdateargs);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_SETENABLE, cthulhu_ee_setenable);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_ADD, cthulhu_ee_add);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_DELETE, cthulhu_ee_delete);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_RESTART, cthulhu_ee_restart);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_MODIFYCONSTRAINTS, cthulhu_ee_modifyconstraints);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_STATS, cthulhu_ee_stats);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_STATS, cthulhu_eu_stats);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EU_UPTIME, cthulhu_eu_uptime);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_DESCRIPTION, cthulhu_ee_description);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_INIT, cthulhu_adapter_init);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_MODIFYAVAILABLEROLES, cthulhu_ee_modifyavailableroles);
    amxm_module_add_function(mod, TIMINGILA_CONTAINER_EE_READPARAM, cthulhu_ee_readparam);
    init_slots();
    wait_and_connect_cthulhu();

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< timingila-cthulhu constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-cthulhu destructor\n");
    amxb_unsubscribe(amxb_bus_ctx_cthulhu, CTHULHU, cthulhu_notify_handler, NULL);
    remove_slots();
    cleanup_cthulhu_command_llist();
    SAH_TRACEZ_INFO(ME, "<<< timingila-cthulhu destructor\n");
    return 0;
}
